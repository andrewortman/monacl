#ifndef __MONACL_COMPUTEMANAGER__
#define __MONACL_COMPUTEMANAGER__

#include "global.hpp"
#include "computation/device.hpp"
#include "computation/fitness_job.hpp"
#include "computation/render_job.hpp"
#include "computation/difference_map_job.hpp"
#include "data/reconstruction.hpp"
#include "omp.h"


class ComputeManager
{	public:
		static vector<Reconstruction> pool;
		
		static vector<Device> devices;
		
		static vector<cl_kernel> fitness_kernels;
		static vector<cl_kernel> render_kernels;
		static vector<cl_kernel> difference_map_kernels;
		
		static vector<cl_mem> target_devices;
	
		static bool UploadTargetImageToDevice(Device &dev, rgba_t *target, int size, cl_mem &target_out); 
		static void FreeTargetFromDevice(cl_mem &target);
		

		static void Initialize(vector<Device> devices, string source_code_path, rgba_t* target);
		static void Release();
	
		static vector<cl_float> calculateFitnesses(vector<Reconstruction> &pool);
		static vector<rgba_t> renderImage(Reconstruction recon);
		static vector<float> calculateDifferenceMap(Reconstruction recon);
};

#endif

/* New algorithm for load balancing:
	First start off with each device having an RPM proportional to the number of cores
	Next, determine the start and end of each device. This happens in one thread
	last spot = 0;
	For each device except last one
	{
		length=round(pool_size*rpm), start = last_spot; last_spot = start+length;
		sum_lengths += length
	}
	last_device.end = length;
	
	execute on each device
	if failure, remove device from list
	recalculate rpms
	if failure, start over
*/	
	
/*
General Algorithm (old) for Load Balancing:
	First start off with some minimum chunk amount (like 50 members of population)
	Start a timer for each device
	Run each chunk through the devices
	Stop the timer when a device finishes.
	
	new_size = (current_size*target_time/execution_time);
	
	Determine the amount of time offset from target time (ie, 1 minute)
	If the CPU took 4 minutes then
		give a chunk target/result * the size = 1/4 = .25x
		
	if the GPU took 20 seconds
		give a chunk target/result * the size (1/(20/60)) = 60/20 = 3 x
		
	Lets not completely extrapolate by that much though (non linearity problems)
		new_size = current_size + (new_size-current_size)*damper
		damper = .5~.8 or so
*/
