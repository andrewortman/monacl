#define MAX_UCHAR 	 255

#define BACKGROUND {MAX_UCHAR,MAX_UCHAR,MAX_UCHAR,MAX_UCHAR};


#ifndef ALIAS_AMOUNT 
	#define ALIAS_AMOUNT 2
#endif

#ifndef WIDTH
	#error WIDTH not specificed during build.
#endif

#ifndef HEIGHT
	#error HEIGHT not specified during build.
#endif

#ifndef NUM_VERTICES 
	#error NUM_VERTICES not specified during build.
#endif

#ifndef MAX_POLYGONS
	#error MAX_POLYGONS not specified during build.
#endif

#pragma OPENCL EXTENSION cl_khr_byte_addressable_store: enable

typedef struct {
	uchar r,g,b,a;
} rgba_t;

typedef struct  {
	float x, y;
} vertex_t;

typedef struct {
	vertex_t verticies[NUM_VERTICES]; //32 bit each
	rgba_t	 color;	//32 bit
} polygon_t;

typedef struct  { 
	polygon_t 	polygons[MAX_POLYGONS]; //32*NUM_VERT*MAX_POLY + 32 bits
	uint		num_polygons; 			//32 bits
	float		fitness;				//most recent fitness calculation <--| These two parameters are not used
	bool		validated;				//^ was calculated succesfully   <---| in the kernels. This just to match alignment
} reconstruction_t; //seperate on 128bit boundries for GPU optimization


//prototypes
inline bool polygon_contains_point(polygon_t poly, vertex_t coord);
inline rgba_t mix_color(rgba_t top, rgba_t bottom);
inline rgba_t render_pixel(uint xx, uint yy, __local reconstruction_t* recon);

//Simple algorithm to check to see if a point is inside a polygon
bool polygon_contains_point(polygon_t poly, vertex_t coord)
{
	//code stolen from: http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html#The C Code
	
	int i, j;
	bool c = 0;
	for (i = 0, j = NUM_VERTICES-1; i < NUM_VERTICES; j = i++) 
	{
		if ( ((poly.verticies[i].y>coord.y) != (poly.verticies[j].y>coord.y)) &&
			(coord.x < (poly.verticies[j].x-poly.verticies[i].x) * (coord.y-poly.verticies[i].y) / (poly.verticies[j].y-poly.verticies[i].y) + poly.verticies[i].x) )
				c = !c;
	}
	return c;
}


//Mixes two colors together
rgba_t mix_color(rgba_t top, rgba_t bottom)
{  
	rgba_t c = bottom;
	
	c.r = (top.r*top.a + bottom.r*(MAX_UCHAR-top.a)*bottom.a/MAX_UCHAR)/MAX_UCHAR;
	c.g = (top.g*top.a + bottom.g*(MAX_UCHAR-top.a)*bottom.a/MAX_UCHAR)/MAX_UCHAR;
	c.b = (top.b*top.a + bottom.b*(MAX_UCHAR-top.a)*bottom.a/MAX_UCHAR)/MAX_UCHAR;
	c.a = top.a + (bottom.a*(MAX_UCHAR-top.a))/MAX_UCHAR;
	
	return c;
}

rgba_t render_pixel(uint xx, uint yy, __local reconstruction_t* recon)
{
	// Render specified pixel 
	float x = (float)(xx/(float)WIDTH);
	float y = (float)(yy/(float)HEIGHT);
	
	vertex_t coord = {x,y};
	rgba_t color = BACKGROUND;
	
	for(uint g = 0; g < recon->num_polygons; g++)
	{
		float alias_amount = 0.0;
		
		for(uint k = 0; k < ALIAS_AMOUNT; k++)
		{
			for(uint j = 0; j < ALIAS_AMOUNT; j++)
			{
				
				vertex_t coord_super = coord;
				coord_super.x += (float)((k+1)/((float)(ALIAS_AMOUNT+1)))/((float)WIDTH);
				coord_super.y += (float)((j+1)/((float)(ALIAS_AMOUNT+1)))/((float)HEIGHT);
				if(polygon_contains_point(recon->polygons[g], coord_super))
				{
					alias_amount += (float)1.0f/(ALIAS_AMOUNT*ALIAS_AMOUNT);
				}
			}
		}
		
		rgba_t color_super = recon->polygons[g].color;
		color_super.a *= alias_amount;
		color = mix_color(color_super, color);
	}
	
	return color;
}

__kernel void calculate_fitness(
			__global rgba_t* target,	 	 	//some CL implementations dont support images and since we arent 
											 	//doing anything with images that could see optimization, just use a 1D buffer with rows one after each other
			__global reconstruction_t* recons,  //buffer of possible solutions (reconstruction candidates)
			__global float* fitness_out, 	 	//output array of fitnesses
			__local float* fitness_buffer)	 	//a buffer for faster reduction, size must equal the number of work items per group
{
	unsigned int loc_size = get_local_size(0);
	unsigned int loc_id = get_local_id(0);
	
	fitness_buffer[loc_id] = 0.0f;
	
	__local reconstruction_t recon_buffer;
	recon_buffer = recons[get_group_id(0)]; 	//grab a copy of the recon image we are working on
	
	for(uint i = loc_id; i < WIDTH*HEIGHT; i+=loc_size)
	{
		
		uint x = i%WIDTH;
		uint y = i/WIDTH;
		
		rgba_t color = render_pixel(x, y, &recon_buffer);
		
		//Calculate fitness at this pixel
		float fitness = 0.0f;
		rgba_t target_color = target[i];

		int red_offset = color.r - target_color.r;
		int green_offset = color.g - target_color.g;
		int blue_offset = color.b - target_color.b;

		fitness += (red_offset*red_offset)/((float)MAX_UCHAR);
		fitness += (blue_offset*blue_offset)/((float)MAX_UCHAR);
		fitness += (green_offset*green_offset)/((float)MAX_UCHAR);
		//fitness = sqrt(fitness);
		
		//add it to the image's fitness buffer
		fitness_buffer[loc_id] += fitness;
	
	}
	
//	wait until all pixel fitnesses are calculated before reducing 
	barrier(CLK_LOCAL_MEM_FENCE);
	
	if(loc_id == 0)
	{
		float fitness_buffer_result = 0.0f;
		for(uint i = 0; i < loc_size; i++)
		{
			fitness_buffer_result += fitness_buffer[i];
		}
	
		fitness_out[get_group_id(0)] = fitness_buffer_result;
	}
	
}

__kernel void render_reconstruction(
				__global reconstruction_t* recon, //the list of reconstruction (should already be in device memory)
				__global rgba_t* output) 		 //the raw rgba_t output
{
		
	uint size = get_global_size(0);
	uint dim = get_global_id(0);
	__local reconstruction_t recon_buffer;
	recon_buffer = *recon;
	
	for(uint xy=dim; xy < WIDTH*HEIGHT; xy+=size)
	{	
		uint x = xy%WIDTH;
		uint y = xy/WIDTH;
		
		rgba_t color = render_pixel(x, y, &recon_buffer);
		output[xy] = color;
	}
}

__kernel void get_difference_map(
								 __global rgba_t* target,
								 __global reconstruction_t* recon,
 								 __global float* output,
								 uint grid_x,
								 uint grid_y)
{
	
	__local reconstruction_t recon_buffer;
	recon_buffer = *recon;
	
	uint dim = get_global_id(0);
	
	uint block_x = dim%grid_x;
	uint block_y = dim/grid_x;
	
	uint start_x = (block_x * WIDTH) / grid_x;
	uint end_x = (start_x*grid_x + WIDTH)/grid_x;
	if(end_x > WIDTH) end_x = WIDTH;
	
	uint start_y = (block_y * HEIGHT) / grid_y;
	uint end_y = (start_y*grid_y + HEIGHT) / grid_y;
	if(end_y > HEIGHT) end_y = HEIGHT;
	
	float fitness_sum = 0.0f;
	uint x,y;
	for( x=start_x; x < end_x; x++)
	{
		for( y = start_y; y < end_y; y++)
		{
			rgba_t color = render_pixel(x, y, &recon_buffer);
			
			rgba_t target_col = target[y*WIDTH+x];
			
			float red_offset = target_col.r - color.r;
			float green_offset = target_col.g - color.g;
			float blue_offset = target_col.b - color.b;
			//fitness_sum += 1;
			fitness_sum += (red_offset*red_offset)/((float)MAX_UCHAR);
			fitness_sum += (blue_offset*blue_offset)/((float)MAX_UCHAR);
			fitness_sum += (green_offset*green_offset)/((float)MAX_UCHAR);
		}
	}
	if((end_y-start_y) == 0 || (end_x-start_x) == 0) output[dim] = 0.0f;
	else output[dim] = fitness_sum;
}
