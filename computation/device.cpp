#include "computation/device.hpp"

void error_notify(const char *errinfo, const void* priv, size_t cb, void* user)
{
	DEBUG("ERROR: %s", errinfo);
}

Device::Device(cl_device_id device)
{
	this->device_initialized = CL_FALSE;
	this->id = device;
	
	clGetDeviceInfo(device, CL_DEVICE_PLATFORM, sizeof(cl_platform_id), &this->platform_id, NULL);
	clGetDeviceInfo(device, CL_DEVICE_TYPE, sizeof(cl_device_type), &this->type, NULL);
	
	this->initialize();
	
	this->setRPM(0.0f); //set to a default RPM of 0
	this->setStartPosition(0);
	this->setEndPosition(0);
	
	DEBUG("Device initialized: %p", this);
}

void Device::release()
{
	if(!this->isInitialized())
	{
		return;
	}
	
	clReleaseCommandQueue(this->command_queue);
	clReleaseContext(this->context);
	DEBUG("Released Device %p", this);
}

bool Device::buildProgram(string source, string build_options, cl_program &program_out)
{
	/* Create the program from source */
	const char* source_code = source.c_str();
	cl_int errcode;
	cl_program prog = clCreateProgramWithSource(this->getContext(), 1, &source_code, NULL, &errcode);
	if(errcode != 0)
	{
		DEBUG("Could not create program from source for device %p (errcode %i)", this, errcode);
		return CL_FALSE;
	}
	
	DEBUG("Created program from source for device %p", this);
	/* Build the program for the device */
	errcode = clBuildProgram(prog, 1, &this->id, build_options.c_str(), NULL, NULL);
	if(errcode != 0)
	{
		DEBUG("Could not build program for device %p (errcode %i)", this, errcode);
		unsigned char build_log[5000];
		clGetProgramBuildInfo(prog, this->id, CL_PROGRAM_BUILD_LOG, sizeof(unsigned char)*5000, build_log, NULL);
		
		DEBUG("Build log: %s", build_log);
		return CL_FALSE;
	}
	DEBUG("Built source for device %p", this);
	
	//build succesfull!
	program_out = prog;
	return CL_TRUE;
}

void Device::initialize()
{	
	cl_bool is_avaliable = CL_FALSE;
	clGetDeviceInfo(this->id, CL_DEVICE_AVAILABLE, sizeof(cl_bool), &is_avaliable, NULL);
	
	if(!is_avaliable)
	{
		DEBUG("Warning: Device not avaliable. No context or queue created.");
		return;
	}
	
	if(this->isInitialized())
	{
		DEBUG("Warning: Device already initialized.");
		return;
	}
	
	//create a context
	cl_context_properties context_properties[] = {CL_CONTEXT_PLATFORM, (cl_context_properties)this->platform_id, 0};
	cl_int errcode;
	this->context = clCreateContext(context_properties, 1, &this->id, error_notify, NULL, &errcode);
	
	if(errcode != CL_SUCCESS)
	{
		DEBUG("Could not create context for device. Error code %i", errcode);
		return;
	}
    
    DEBUG("Created context!");
	
    
    //create a in-order command queue
	this->command_queue = clCreateCommandQueue(this->context, this->id, CL_QUEUE_PROFILING_ENABLE, &errcode);
	
	if(errcode != CL_SUCCESS)
	{
		DEBUG("Could not create command queue for device. Error code %i", errcode);
		clReleaseContext(this->context);
		return;
	}
    
    DEBUG("Created queue!");
	
	this->device_initialized = CL_TRUE;
	this->validate();
}

bool Device::isCPU()
{
	return this->type == CL_DEVICE_TYPE_CPU;
}


bool Device::isGPU()
{
	return this->type == CL_DEVICE_TYPE_GPU;
}

cl_uint Device::getCores()
{
	cl_uint to_return = 0;
	HANDLE_CL_ERROR(clGetDeviceInfo(this->id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &to_return, NULL));
	return to_return;
}

std::string Device::getName()
{
	char name_buffer[100];
	clGetDeviceInfo(this->id, CL_DEVICE_NAME, sizeof(char)*100, name_buffer, NULL);
	return std::string(name_buffer);
}

//STATIC METHODS

vector<Device> Device::GetAvaliableDevices(cl_device_type device_type)
{
	vector<Device> device_list;
	
	cl_uint num_platforms;
	HANDLE_CL_ERROR(clGetPlatformIDs(0, NULL, &num_platforms));
	
	if(num_platforms == 0) FAULT("No OpenCL Implementation found. Please install OpenCL before continuing.");
	
	DEBUG("%i platforms found on this system.", num_platforms);
	
	//create a platform list
	cl_platform_id *platforms = new cl_platform_id[num_platforms];
	HANDLE_CL_ERROR(clGetPlatformIDs(num_platforms, platforms, NULL));
	
	//go through the platform list and create devices 
	for(unsigned int i = 0; i < num_platforms; i++)
	{
		cl_uint num_devices;
		
		//grab the number of devices for this platform
		clGetDeviceIDs(platforms[i], device_type, 0, NULL, &num_devices);
		DEBUG("Platform %i has %i devices.",i+1, num_devices)
		if(num_devices == 0) continue;
		
		//grab the device id list
		cl_device_id *devices = new cl_device_id[num_devices];
		HANDLE_CL_ERROR(clGetDeviceIDs(platforms[i], device_type, num_devices, devices, NULL));
		
		//for each device, create a device object and add it to the device_list
		for(unsigned int x = 0; x < num_devices; x++)
		{
			DEBUG("Creating device for platform %i, device %i",i+1,x+1);
			Device dev(devices[x]);
			DEBUG("Device created for %s device '%s'", dev.isGPU() ? "GPU" : "CPU", dev.getName().c_str());
			
			if(dev.isInitialized())
				device_list.push_back(dev);
			else
				DEBUG("Device '%s' not avaliable for use. Not pushed onto avaiable device vector.", dev.getName().c_str());
		}
		delete[] devices; //we dont need the device id list anymore
	}
	delete[] platforms; //we dont need the platform id list anymore
	
	return device_list;
}

void Device::ReleaseDevices(vector<Device> &devices)
{
	for(unsigned int i = 0; i < devices.size(); i++)
	{
		devices[i].release();
	}
	devices.clear();
}

bool Device::getKernelFromProgram(cl_program program, string kernel_name, cl_kernel &kernel_out)
{
	cl_int errcode;
	cl_kernel kern = clCreateKernel(program, kernel_name.c_str(), &errcode);
	if(errcode != 0)
	{
		DEBUG("Could not create kernel '%s' (errcode %i)", kernel_name.c_str(), errcode);
		return CL_FALSE;
	}
	
	kernel_out = kern;
	return CL_TRUE;
}
