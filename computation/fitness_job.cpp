#include "computation/fitness_job.hpp"

FitnessJob::FitnessJob(vector<Reconstruction> &pool)
{
	this->pool = &pool;
	this->results.resize(this->pool->size());
	std::fill(results.begin(), results.end(), 0.0f);
	this->execution_time = 0.0;
}

cl_bool FitnessJob::execute(Device &dev, cl_kernel kernel, cl_mem &target_device)
{
	//Executes the job on a device with the given kernel
	
	cl_int errcode;
	cl_uint pool_size = dev.getLength();
	cl_uint pool_start = dev.getStartPosition();
	DEBUG("Pool size = %u, start = %u", pool_size, pool_start)
	//First allocate the buffers on the device to hold the pool
	cl_mem reconstruction = clCreateBuffer(dev.getContext(), CL_MEM_READ_ONLY, pool_size*sizeof(reconstruction_t), NULL, &errcode);
	if(errcode)
	{
		DEBUG("%s (err code %i on device '%p [%s]')", "Could not create reconstruction buffer", (int)errcode, &dev, dev.getName().c_str());
		return CL_FALSE;
	}
	
	//Next, allocate the buffers for the output
	cl_mem output_device = clCreateBuffer(dev.getContext(), CL_MEM_WRITE_ONLY, pool_size*sizeof(cl_float), NULL, &errcode);
	if(errcode)
	{
		DEBUG("%s (err code %i on device '%p [%s]')", "Could not create output buffer", (int)errcode, &dev, dev.getName().c_str());
		return CL_FALSE;
	}
	
	
	/* Now determine the work size for this device */
	
	//get the maximum work group size of this kernel
	size_t wg_size = 0;
	HANDLE_CL_ERROR(clGetKernelWorkGroupInfo(kernel, dev.getID(), CL_KERNEL_WORK_GROUP_SIZE, sizeof(size_t),&wg_size,NULL));

	/* Define the dimensions for this problem */
	size_t local_dim[1], global_dim[1];
	if(dev.isCPU())
	{
		wg_size = 1;
	}
	
	local_dim[0] = wg_size;
	global_dim[0] = wg_size*pool_size;
	
	
	//Set the kernel's arguments
	HANDLE_CL_ERROR(clSetKernelArg(kernel, 0, sizeof(cl_mem), &target_device));
	HANDLE_CL_ERROR(clSetKernelArg(kernel, 1, sizeof(cl_mem), &reconstruction));
	HANDLE_CL_ERROR(clSetKernelArg(kernel, 2, sizeof(cl_mem), &output_device));
	HANDLE_CL_ERROR(clSetKernelArg(kernel, 3, sizeof(cl_float)*wg_size, NULL));
	
	
	/* Now queue up the OpenCL commands */
	
	cl_event upload, run, download; //create some events for profiling
	
	/* Write the reconstruction buffer to the device */
	errcode = clEnqueueWriteBuffer(dev.getCommandQueue(), reconstruction, CL_FALSE, 0,sizeof(reconstruction_t)*pool_size, &((*pool)[0]) + dev.getStartPosition(), 0, NULL, &upload);
	if(errcode)
	{
		DEBUG("%s (err code %i on device '%p [%s]')", "Could not write reconstruction buffer to device", (int)errcode, &dev, dev.getName().c_str());
		return CL_FALSE;
	}
	
	/* Actually execute the kernel */
	errcode = clEnqueueNDRangeKernel(dev.getCommandQueue(), kernel, 1, NULL, global_dim, local_dim, 0, NULL, &run);
	if(errcode)
	{
		DEBUG("%s (err code %i on device '%p [%s]')", "Could not execute on the device", (int)errcode, &dev, dev.getName().c_str());
		return CL_FALSE;
	}
	
	/* Read the results from the device (Blocking is true!) */
	errcode = clEnqueueReadBuffer(dev.getCommandQueue(), output_device, CL_TRUE, 0, pool_size*sizeof(cl_float), &(this->results[0]), 0, NULL, &download);
	if(errcode)
	{
		DEBUG("%s (err code %i on device '%p [%s]')", "Could not read data from device", (int)errcode, &dev, dev.getName().c_str());
		return CL_FALSE;
	}
	
	/* Release those memory objects we dont need */
	errcode = clReleaseMemObject(reconstruction);
	errcode |= clReleaseMemObject(output_device);
	
	if(errcode)
	{
		DEBUG("%s (err code %i on device '%p [%s]')", "Could not release memory on device!", (int)errcode, &dev, dev.getName().c_str());
		return CL_FALSE;
	}
	
	/* Gather profiling results */
	DEBUG("Execution of fitness job %p (size %i) complete!", this, pool_size);
	
	cl_ulong start,end;
	
	errcode = clGetEventProfilingInfo(upload, CL_PROFILING_COMMAND_QUEUED, sizeof(cl_ulong), &start, NULL);
	errcode |= clGetEventProfilingInfo(download, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end, NULL);
	
	if(errcode)
	{
		DEBUG("%s (err code %i on device '%p [%s]')", "Could not profile result", (int)errcode, &dev, dev.getName().c_str());
		return CL_FALSE;
	}
	
	this->execution_time = (end-start);
	DEBUG("Execution of fitness job of size %i on device '%s' took %f milliseconds.", (int)pool_size, dev.getName().c_str(), (this->execution_time)/(1000000.0f));
	
	//Execution succesfull!
	return CL_TRUE;
}
