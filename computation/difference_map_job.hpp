#ifndef __MONACL_DIFFERENCE_MAP_JOB_HPP__
#define __MONACL_DIFFERENCE_MAP_JOB_HPP__

#include "global.hpp"
#include "computation/device.hpp"
#include "data/reconstruction.hpp"

class DifferenceMapJob
{
	private:
		Reconstruction recon;
		vector<float> results;	
		cl_ulong execution_time;
		
	public:
		DifferenceMapJob(Reconstruction &recon);
		
		cl_bool execute(Device &dev, cl_kernel kernel, cl_mem &target_device);
		
		vector<float> getResults() { return results; };
		
		cl_ulong getExecutionTime() { return execution_time; }
		
		static const char* getKernelName() { return "get_difference_map"; }
};

#endif
