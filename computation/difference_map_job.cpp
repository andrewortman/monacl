#include "computation/difference_map_job.hpp"

DifferenceMapJob::DifferenceMapJob(Reconstruction &recon)
{
	this->recon = recon;
	
	this->results.resize(Parameters::diff_grid_x*Parameters::diff_grid_y);
	std::fill(results.begin(), results.end(), 0.0f);
	this->execution_time = 0.0;
}

cl_bool DifferenceMapJob::execute(Device &dev, cl_kernel kernel, cl_mem &target_mem)
{
	//Executes the job on a device with the given kernel
	
	cl_int errcode;
	
	//First allocate the buffers on the device
	cl_mem reconstruction = clCreateBuffer(dev.getContext(), CL_MEM_READ_ONLY, sizeof(reconstruction_t), NULL, &errcode);
	if(errcode)
	{
		DEBUG("%s (err code %i on device '%p [%s]')", "Could not create reconstruction buffer", (int)errcode, &dev, dev.getName().c_str());
		return CL_FALSE;
	}
	
	//Next, allocate the buffers for the output
	cl_mem output_device = clCreateBuffer(dev.getContext(), CL_MEM_WRITE_ONLY, Parameters::diff_grid_x*Parameters::diff_grid_y*sizeof(cl_float), NULL, &errcode);
	if(errcode)
	{
		DEBUG("%s (err code %i on device '%p [%s]')", "Could not create output buffer", (int)errcode, &dev, dev.getName().c_str());
		return CL_FALSE;
	}
	
	
	/* Now determine the work size for this device */
	
	/* Define the dimensions for this problem */
	size_t local_dim[1], global_dim[1];
	
	local_dim[0] = 1;
	global_dim[0] = Parameters::diff_grid_x*Parameters::diff_grid_y;
	
	
	//Set the kernel's arguments
	HANDLE_CL_ERROR(clSetKernelArg(kernel, 0, sizeof(cl_mem), &target_mem));
	HANDLE_CL_ERROR(clSetKernelArg(kernel, 1, sizeof(cl_mem), &reconstruction));
	HANDLE_CL_ERROR(clSetKernelArg(kernel, 2, sizeof(cl_mem), &output_device));
	HANDLE_CL_ERROR(clSetKernelArg(kernel, 3, sizeof(cl_uint), &Parameters::diff_grid_x));
	HANDLE_CL_ERROR(clSetKernelArg(kernel, 4, sizeof(cl_uint), &Parameters::diff_grid_y));
	
	
	/* Now queue up the OpenCL commands */
	
	cl_event upload, run, download; //create some events for profiling
	
	/* Write the reconstruction buffer to the device */
	errcode = clEnqueueWriteBuffer(dev.getCommandQueue(), reconstruction, CL_FALSE, 0,sizeof(reconstruction_t), &(this->recon), 0, NULL, &upload);
	if(errcode)
	{
		DEBUG("%s (err code %i on device '%p [%s]')", "Could not write reconstruction buffer to device", (int)errcode, &dev, dev.getName().c_str());
		return CL_FALSE;
	}
	
	/* Actually execute the kernel */
	errcode = clEnqueueNDRangeKernel(dev.getCommandQueue(), kernel, 1, NULL, global_dim, local_dim, 0, NULL, &run);
	if(errcode)
	{
		DEBUG("%s (err code %i on device '%p [%s]')", "Could not execute on the device", (int)errcode, &dev, dev.getName().c_str());
		return CL_FALSE;
	}
	
	/* Read the results from the device (Blocking is true!) */
	errcode = clEnqueueReadBuffer(dev.getCommandQueue(), output_device, CL_TRUE, 0, Parameters::diff_grid_x*Parameters::diff_grid_y*sizeof(float), &(this->results[0]), 0, NULL, &download);
	if(errcode)
	{
		DEBUG("%s (err code %i on device '%p [%s]')", "Could not read data from device", (int)errcode, &dev, dev.getName().c_str());
		return CL_FALSE;
	}
	
	/* Release those memory objects we dont need */
	errcode = clReleaseMemObject(reconstruction);
	errcode |= clReleaseMemObject(output_device);
	
	if(errcode)
	{
		DEBUG("%s (err code %i on device '%p [%s]')", "Could not release memory on device!", (int)errcode, &dev, dev.getName().c_str());
	}
	
	/* Gather profiling results */
	DEBUG("Execution of difference map job %p complete!", this);
	
	cl_ulong start,end;
	
	errcode = clGetEventProfilingInfo(upload, CL_PROFILING_COMMAND_QUEUED, sizeof(cl_ulong), &start, NULL);
	errcode |= clGetEventProfilingInfo(download, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end, NULL);
	
	if(errcode)
	{
		DEBUG("%s (err code %i on device '%p [%s]')", "Could not profile result", (int)errcode, &dev, dev.getName().c_str());
		return CL_FALSE;
	}
	
	this->execution_time = (end-start);
	DEBUG("Execution of difference map job on device '%s' took %f milliseconds", dev.getName().c_str(), (this->execution_time)/(1000000.0f));
	
	//Execution succesfull!
	return CL_TRUE;
}
