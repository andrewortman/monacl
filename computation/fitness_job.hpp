#ifndef __MONACL_FITNESS_JOB_HPP__
#define __MONACL_FITNESS_JOB_HPP__

#include "global.hpp"
#include "data/reconstruction.hpp"
#include "computation/device.hpp"

class FitnessJob
{
	private:
		vector<Reconstruction> *pool;
		vector<cl_float> results;		
		cl_ulong execution_time;
		
	public:
		FitnessJob(vector<Reconstruction> &pool);
		
		cl_bool execute(Device &dev, cl_kernel kernel, cl_mem &target_device);
		
		vector<cl_float> getResults() { return results; };
		
		cl_ulong getExecutionTime() { return execution_time; }
		
		static const char* getKernelName() { return "calculate_fitness"; }
};

#endif
