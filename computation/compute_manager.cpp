#include "computation/compute_manager.hpp"

//define the static variables
vector<Reconstruction> ComputeManager::pool;
vector<Device> ComputeManager::devices;
vector<cl_kernel> ComputeManager::fitness_kernels;
vector<cl_kernel> ComputeManager::render_kernels;
vector<cl_kernel> ComputeManager::difference_map_kernels;
vector<cl_mem> ComputeManager::target_devices;

bool ComputeManager::UploadTargetImageToDevice(Device &dev, rgba_t *target, int size, cl_mem &target_out)
{
	if(!dev.isInitialized())
	{
		DEBUG("Device %p not initialized yet!", &dev);
		return false;
	}
	
	cl_int errcode;
	cl_mem mem = clCreateBuffer(dev.getContext(), CL_MEM_READ_ONLY, sizeof(rgba_t)*size, NULL, &errcode);
	if(errcode != 0)
	{
		DEBUG("Could not allocate buffer on device %p (errcode %i)", &dev, errcode);
		return false;
	}
	
	errcode = clEnqueueWriteBuffer(dev.getCommandQueue(), mem, CL_TRUE, 0, sizeof(rgba_t)*size, target, 0, NULL, NULL);
	if(errcode != 0)
	{
		DEBUG("Could not write buffer to device %p (errcode %i)", &dev, errcode);
		return false;
	}
	
	target_out = mem;
	DEBUG("Uploaded target %p to device %p", &target_out, &dev);
	
	return true;
}

void ComputeManager::FreeTargetFromDevice(cl_mem &target)
{
	DEBUG("Releasing target %p from device..", &target);
	clReleaseMemObject(target);
}

void ComputeManager::Initialize(vector<Device> _devices, string source_code_path, rgba_t* target)
{	
	INFO("Intializing ComputeManager..");
	std::ifstream srcfile(source_code_path.c_str(), ifstream::in);
	if(!srcfile.is_open())
	{
		FAULT("Could not find source file to compile.");
	}
	
	string source_code;
	source_code.assign((std::istreambuf_iterator<char>(srcfile)),
	            std::istreambuf_iterator<char>());	
	
	srcfile.close();

	//Ok, first, lets upload the target to each device
	target_devices.reserve(_devices.size());
	for(vector<Device>::iterator dev = _devices.begin(); dev < _devices.end(); dev++)
	{
		cl_mem targ_out;
		if(!ComputeManager::UploadTargetImageToDevice(*dev, target, Parameters::image_width*Parameters::image_height, targ_out))
		{
			INFO("Could not upload target to device '%s' - not adding device to list", dev->getName().c_str());
			continue;
		}
		
		DEBUG("Uploaded target to device");
		
		
		//build the programs
		cl_program prog;
		
		stringstream bo;
		bo << "-D WIDTH=" << Parameters::image_width << " -D HEIGHT=" << Parameters::image_height << " -D MAX_POLYGONS="<< MAX_POLYGONS <<" -D NUM_VERTICES="<<NUM_VERTICES;
		DEBUG("Build options: %s", bo.str().c_str());
		
		if(!dev->buildProgram(source_code, bo.str(), prog))
		{
			INFO("Could not build source code for device '%s' - not adding device to list", dev->getName().c_str());
			continue;
		}
		
		cl_kernel fitness_kern;
		if(!Device::getKernelFromProgram(prog, FitnessJob::getKernelName(), fitness_kern))
		{
			INFO("Could not extract fitness kernel from source for device '%s' - not adding device to list", dev->getName().c_str());
			continue;
		}
		
		cl_kernel render_kern;
		if(!Device::getKernelFromProgram(prog, RenderJob::getKernelName(), render_kern))
		{
			INFO("Could not extract render kernel from source for device '%s' - not adding device to list", dev->getName().c_str());
			continue;
		}
		
		cl_kernel difference_map_kern;
		if(!Device::getKernelFromProgram(prog, DifferenceMapJob::getKernelName(), difference_map_kern))
		{
			INFO("Could not extract difference map kernel from source for device '%s' - not adding device to list", dev->getName().c_str());
			continue;
		}
		
		INFO("Device '%s' avaliable for use", dev->getName().c_str());
		ComputeManager::fitness_kernels.push_back(fitness_kern);
		ComputeManager::render_kernels.push_back(render_kern);
		ComputeManager::difference_map_kernels.push_back(difference_map_kern);
		ComputeManager::devices.push_back(*dev);
		ComputeManager::target_devices.push_back(targ_out);
	}	
	if(ComputeManager::devices.size() == 0) FAULT("No devices avaliable!");
	
	//now initialize the devices with an RPM proportional to number of cores
	//first find total number of cores in the system across all devices
	cl_uint total_number_of_cores = 0.0;
	for(cl_uint i = 0; i < ComputeManager::devices.size(); i++) total_number_of_cores += ComputeManager::devices[i].getCores();
	
	DEBUG("Total number of cores detected: %u", total_number_of_cores);
	for(cl_uint i = 0; i < ComputeManager::devices.size(); i++)
	{
		cl_float rpm = (float)ComputeManager::devices[i].getCores()/total_number_of_cores;
		DEBUG("Device '%s' RPM: %f", ComputeManager::devices[i].getName().c_str(), rpm);
		ComputeManager::devices[i].setRPM(rpm);
	}
	INFO("Devices intialized - ready for execution.");
}

vector<cl_float> ComputeManager::calculateFitnesses(vector<Reconstruction> &pool)
{
	if(pool.size() == 0)
	{
		vector<cl_float> to_return;
		return to_return;
	}
	DEBUG("Starting %i number of threads for device management for %i reconstructions", (int)devices.size(), (int)pool.size());
	vector<cl_float> results_ret;
	
	results_ret.resize(pool.size());
	
	do
	{
		Reconstruction::invalidateAll(pool);
		cl_uint start = 0;
		for(cl_uint i = 0; i < devices.size(); i++)
		{
			devices[i].setStartPosition(start);
			if(i == devices.size()-1)
			{
				devices[i].setEndPosition(pool.size());
			}
			else
			{
				cl_uint new_start = start + pool.size()*devices[i].getRPM();
				devices[i].setEndPosition(new_start);
				start = new_start;
			}
			
			DEBUG("Device %i start_position=%u end_position=%u (length=%u)", i, devices[i].getStartPosition(), devices[i].getEndPosition(), devices[i].getLength())
		}
		
		
		#pragma omp parallel num_threads(devices.size()) shared(results_ret)
		{
			
			cl_uint t_num = omp_get_thread_num();
			
			FitnessJob job(pool);
			if(devices[t_num].getLength() <= 0)
			{
				DEBUG("Device has nonpositive device size. Breaking out");
			}
			else
			{
				DEBUG("Starting job..");
				if(job.execute(devices[t_num], fitness_kernels[t_num], target_devices[t_num]))
				{
					vector<cl_float> results_dev = job.getResults();
					cl_uint start = devices[t_num].getStartPosition();
					cl_uint end = devices[t_num].getEndPosition();
					for(cl_uint i = start; i < end; i++)
					{
						pool[i].fitness = results_dev[i-start];
						results_ret[i] = pool[i].fitness;
						pool[i].validate();
					}
				
					cl_ulong execution_time = job.getExecutionTime();
					devices[t_num].setAPM((float)devices[t_num].getLength()/(execution_time/(1.0e9)));
					INFO("Job completed on %s with APM of %f recons/second", devices[t_num].getName().c_str(), devices[t_num].getAPM());
				}
				else
				{
					FAULT("Device failure with device %s", devices[omp_get_thread_num()].getName().c_str());
				}
			}
		}

		//Recalculate RPM for each device
		cl_float total_apm = 0.0f;
		for(unsigned int i = 0; i < devices.size(); i++) total_apm+=devices[i].getAPM();
		for(unsigned int i = 0; i < devices.size(); i++)
		{
			devices[i].setRPM(devices[i].getAPM()/total_apm);
		}
		
	}
	while(!Reconstruction::isValidatedAll(pool));
	return results_ret;
}

vector<rgba_t> ComputeManager::renderImage(Reconstruction recon)
{
	//Just pick the first device to render for now, no need to profiling yet
	RenderJob job(recon, Parameters::image_width, Parameters::image_height);
	if(!job.execute(devices[0], render_kernels[0]))
	{
		//TODO: Make this more resilient to faults
		FAULT("Could not render image :(");
	}
	
	return job.getResults();
}

vector<float> ComputeManager::calculateDifferenceMap(Reconstruction recon)
{
	DifferenceMapJob job(recon);
	if(!job.execute(devices[0], difference_map_kernels[0], target_devices[0]))
	{
		FAULT("Could not calculate difference map :(");
	}
	
	return job.getResults();
}

void ComputeManager::Release()
{
	for(unsigned int i = 0; i < devices.size(); i++)
	{
		FreeTargetFromDevice(target_devices[i]);
	}
	
	Device::ReleaseDevices(devices);
}
	
