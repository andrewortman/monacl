#ifndef __MONACL_RENDER_JOB_HPP__
#define __MONACL_RENDER_JOB_HPP__

#include "global.hpp"
#include "data/reconstruction.hpp"
#include "computation/device.hpp"

class RenderJob
{
	private:
		Reconstruction recon;
		vector<rgba_t> results;	
		cl_uint width, height;
		cl_ulong execution_time;
		
	public:
		RenderJob(Reconstruction &recon, cl_uint width, cl_uint height);
		
		cl_bool execute(Device &dev, cl_kernel kernel);
		
		vector<rgba_t> getResults() { return results; };
		
		cl_ulong getExecutionTime() { return execution_time; }
		
		static const char* getKernelName() { return "render_reconstruction"; }
};

#endif
