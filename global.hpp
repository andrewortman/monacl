#ifndef __MONACL_GLOBAL_HPP__
#define __MONACL_GLOBAL_HPP__

#include <vector> 
#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <sstream>
#include <iomanip>
#include <stdint.h>
#include <algorithm>
#include <sys/time.h>

using namespace std;

//number of vertices per polygon
#define NUM_VERTICES 5
//number of polygons
#define MAX_POLYGONS 250

#if defined(__APPLE__) || defined(__MACOSX)
#include <OpenCL/opencl.h>
#else
#include <CL/opencl.h>
#endif

#ifdef __MONACL_DEBUG__
#define DEBUG(message,...) {\
	printf("DEBUG %s [%s:%d] => ", __FUNCTION__, __FILE__, __LINE__); \
	printf(message, ##__VA_ARGS__); \
	printf("\r\n"); }
#else
#define DEBUG(message, ...) ;
#endif

#define HANDLE_CL_ERROR(A) { if(A != CL_SUCCESS) FAULT("OpenCL Error: Code %i", (int)A); }
#define HANDLE_MPI_ERROR(A) { if(A != MPI_SUCCESS) FAULT("OpenMPI Error: Code %i", (int)A); }

#ifdef __MONACL_INFO__
#define INFO(message, ...) {\
		printf("INFO => "); \
		printf(message, ##__VA_ARGS__); \
		printf("\r\n"); }
#else
#define INFO(message, ...) ;
#endif

#define OUTPUTLINE(message, ...) { \
		printf(message, ##__VA_ARGS__);\
		printf("\r\n");	}
		
#define FAULT(message, ...) { \
		printf("FAULT! %s [%s:%d] => ", __FUNCTION__, __FILE__, __LINE__); \
		printf(message, ##__VA_ARGS__); \
		printf("\r\n"); \
		exit(1); }
		
#define ASSERT(condition, message, ...) { \
		if(!(condition)) { \
		printf("ASSERT FAIL! %s [%s:%d] => ", __FUNCTION__, __FILE__, __LINE__); \
		printf(message, ##__VA_ARGS__); \
		printf("\r\n"); \
		exit(1); } }
//some random number helpers (stolen from gpulisa)

//generates a random float from 0 to 1.0
#define RAND_RF(A) rand_r(&A)/(float)RAND_MAX

//generates a random float from [B, C]
#define RAND_RF_RANGE(A,B,C) (((float)(C-B)*(rand_r(&A)/(float)RAND_MAX))+(float)B)

//generates a random int from [0, RAND_MAX]
#define RAND_RI(A) rand_r(&A)

//generates a random int in [B,C)
#define RAND_RI_RANGE(A,B,C) ((C-B)==0? C : (int)(rand_r(&A)%(C-B) + B))

//generates a boolean with a chance of true being B (a float)
#define RAND_RB_CHANCE(A,B) ((float)(rand_r(&A)/(float)RAND_MAX) < B)

#endif


