#ifndef __MONACL_IMAGE_RENDERER_HPP__
#define __MONACL_IMAGE_RENDERER_HPP__

#include "global.hpp"
#include "computation/compute_manager.hpp"
#include "data/reconstruction.hpp"

class ImageRenderer
{	
public:
	/**
	 * @brief Renders and saves the reconstruction as a PNG file.
	 * @brief recon The reconstruction to render
	 * @param path The filepath to save the image at
	 * @return true if success, false on failure.
	 */
	static cl_bool saveReconstructionToPNG(Reconstruction recon, string path);

	
	/**
	 * @brief Renders and saves the reconstruction as a SVG file.
	 * @brief recon The reconstruction to render
	 * @param path The filepath to save the image at
	 * @return true if success, false on failure.
	 */
	static cl_bool saveReconstructionToSVG(Reconstruction recon, string path);
};
#endif
