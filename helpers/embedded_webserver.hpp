#include "global.hpp"
#include "helpers/mongoose_wrapper.hpp"

using namespace mongoose;

class EmbeddedWebServer : public MongooseServer 
{
public:
	EmbeddedWebServer(): MongooseServer() {}
	virtual ~EmbeddedWebServer() {}    
    void addGenerationToStats(cl_uint gen_num, cl_float best_fitness, cl_uint total_polygons);
    
protected:
	virtual bool handleEvent(ServerHandlingEvent event_code, MongooseConnection &connection, const MongooseRequest &request, MongooseResponse &response);
    
    
private:
    vector<std::string> generationStats;
};	
