#include "parameters.hpp"

cl_float Parameters::color_change_mutation_rate = 0.01;
cl_float Parameters::color_nudge_mutation_rate = 0.05;
cl_float Parameters::color_nudge_max_amount = 0.2;

cl_float Parameters::alpha_change_mutation_rate = 0.01;
cl_float Parameters::alpha_nudge_mutation_rate = 0.05;
cl_float Parameters::alpha_nudge_max_amount = 0.2;

cl_float Parameters::vertex_nudge_far_mutation_rate = 0.001;
cl_float Parameters::vertex_nudge_far_max_amount = 0.6;
cl_float Parameters::vertex_nudge_medium_mutation_rate = 0.005;
cl_float Parameters::vertex_nudge_medium_max_amount = 0.2;
cl_float Parameters::vertex_nudge_close_mutation_rate = 0.01;
cl_float Parameters::vertex_nudge_close_max_amount = 0.1;

cl_float Parameters::polygon_swap_layer_rate = 0.5;
cl_float Parameters::polygon_add_rate = 0.1;
cl_float Parameters::polygon_remove_rate = 0.2;


cl_int Parameters::population_size = 10;
cl_uint Parameters::start_polygon_count = 10;
cl_float Parameters::selection_percentage = 0.25;
cl_float Parameters::elitism_percentage = 0.05;

cl_float Parameters::size_limitation_x=0.25;
cl_float Parameters::size_limitation_y=0.25;
cl_float Parameters::bound_limitation_x=0.2;
cl_float Parameters::bound_limitation_y=0.2;

cl_uint Parameters::image_width = 0;
cl_uint Parameters::image_height = 0;

cl_float Parameters::color_range_r_min = 0.0f;
cl_float Parameters::color_range_r_max = 1.0f;

cl_float Parameters::color_range_g_min = 0.0f;
cl_float Parameters::color_range_g_max = 1.0f;

cl_float Parameters::color_range_b_min = 0.0f;
cl_float Parameters::color_range_b_max = 1.0f;

cl_float Parameters::color_range_a_min = 0.0f;
cl_float Parameters::color_range_a_max = 1.0f;

cl_bool Parameters::color_hint = CL_FALSE;
cl_uint Parameters::diff_grid_x = 16;
cl_uint Parameters::diff_grid_y = 16;
cl_uint Parameters::diff_map_gen_count = 25;


cl_bool Parameters::webserver_enabled = CL_TRUE;
cl_int Parameters::webserver_port = 8080;

cl_float Parameters::redistribution_threshold = 1.0f;

cl_bool Parameters::loadFromFile(const char* filename)
{
	FILE* fp = fopen(filename, "r");
	if(!fp)
	{
		DEBUG("No configuration file (%s) detected - using defaults in settings.cpp",filename);
		return CL_FALSE;
	}
	cl_uint line_no = 0;
	while(!feof(fp))
	{
		line_no++;
		char line[100];
		char name[100];
		float value;
		if(fgets(line, 100, fp) == NULL) continue;
		if(sscanf(line, "%[^=]=%f\r\n", name, &value) != 2)
		{
			DEBUG("Ignoring line number %u", line_no);
			continue;
		}
		
		//Color Mutation Parameters
		if(strcmp(name, "color_change_mutation_rate") == 0)
		{
			Parameters::color_change_mutation_rate = value;
			DEBUG("Set Parameters::color_change_mutation_rate to %f", value);
		}
		else if(strcmp(name, "color_nudge_mutation_rate") == 0)
		{
			Parameters::color_nudge_mutation_rate = value;
			DEBUG("Set Parameters::color_nudge_mutation_rate to %f", value);
		}
		else if(strcmp(name, "color_nudge_max_amount") == 0)
		{
			Parameters::color_nudge_max_amount = value;
			DEBUG("Set Parameters::color_nudge_max_amount to %f", value);
		}
		//Alpha Mutation Parameters
		else if(strcmp(name, "alpha_change_mutation_rate") == 0)
		{
			Parameters::alpha_change_mutation_rate = value;
			DEBUG("Set Parameters::alpha_change_mutation_rate to %f", value);
		}
		else if(strcmp(name, "alpha_nudge_mutation_rate") == 0)
		{
			Parameters::alpha_nudge_mutation_rate = value;
			DEBUG("Set Parameters::alpha_nudge_mutation_rate to %f", value);
		}
		else if(strcmp(name, "alpha_nudge_max_amount") == 0)
		{
			Parameters::alpha_nudge_max_amount = value;
			DEBUG("Set Parameters::alpha_change_max_amount to %f", value);
		}
		
		//Far Vertex Nudge Parameters
		else if(strcmp(name, "vertex_nudge_far_mutation_rate") == 0)
		{
			Parameters::vertex_nudge_far_mutation_rate = value;
			DEBUG("Set Parameters::vertex_nudge_far_mutation_rate to %f", value);
		}
		else if(strcmp(name, "vertex_nudge_far_max_amount") == 0)
		{
			Parameters::vertex_nudge_far_max_amount = value;
			DEBUG("Set Parameters::vertex_nudge_far_max_amount to %f", value);
		}
		
		//Medium Vertex Nudge Parameters
		else if(strcmp(name, "vertex_nudge_medium_mutation_rate") == 0)
		{
			Parameters::vertex_nudge_medium_mutation_rate = value;
			DEBUG("Set Parameters::vertex_nudge_medium_mutation_rate to %f", value);
		}
		else if(strcmp(name, "vertex_nudge_medium_max_amount") == 0)
		{
			Parameters::vertex_nudge_medium_max_amount = value;
			DEBUG("Set Parameters::vertex_nudge_medium_max_amount to %f", value);
		}
		
		//Close Vertex Nudge Parameters
		else if(strcmp(name, "vertex_nudge_close_mutation_rate") == 0)
		{
			Parameters::vertex_nudge_close_mutation_rate = value;
			DEBUG("Set Parameters::vertex_nudge_close_mutation_rate to %f", value);
		}
		else if(strcmp(name, "vertex_nudge_close_max_amount") == 0)
		{
			Parameters::vertex_nudge_close_max_amount = value;
			DEBUG("Set Parameters::vertex_nudge_close_max_amount to %f", value);
		}
		
		//Other mutation rates
		else if(strcmp(name, "polygon_swap_layer_rate") == 0)
		{
			Parameters::polygon_swap_layer_rate = value;
			DEBUG("Set Parameters::polygon_swap_layer_rate to %f", value);
		}
		else if(strcmp(name, "polygon_add_rate") == 0)
		{
			Parameters::polygon_add_rate = value;
			DEBUG("Set Parameters::polygon_add_rate to %f", value);
		}		
		else if(strcmp(name, "polygon_remove_rate") == 0)
		{
			Parameters::polygon_remove_rate = value;
			DEBUG("Set Parameters::polygon_remove_rate to %f", value);
		}
		//Population control
		else if(strcmp(name, "population_size") == 0)
		{
			Parameters::population_size = (cl_int)value;
			DEBUG("Set Parameters::population_size to %i", (cl_int)value);
		}
		else if(strcmp(name, "start_polygon_count") == 0)
		{
			Parameters::start_polygon_count = (cl_uint)value;
			DEBUG("Set Parameters::start_polygon_count to %u", (cl_uint)value);
		}
		else if(strcmp(name, "selection_percentage") == 0)
		{
			Parameters::selection_percentage = value;
			DEBUG("Set Parameters::selection_percentage to %f (which gives %i size)", value, (cl_int)(value*Parameters::population_size));
		}
		else if(strcmp(name, "elitism_percentage") == 0)
		{
			Parameters::elitism_percentage = value;
			DEBUG("Set Parameters::elitism_percentage to %f (which gives %i size)", value, (cl_int)(value*Parameters::population_size));
		}
		
		//Polygon control
		else if(strcmp(name, "size_limitation_x") == 0)
		{
			Parameters::size_limitation_x = value;
			DEBUG("Set Parameters::size_limitation_x to %f", value);
		}
		else if(strcmp(name, "size_limitation_y") == 0)
		{
			Parameters::size_limitation_y = value;
			DEBUG("Set Parameters::size_limitation_y to %f", value);
		}
		else if(strcmp(name, "bound_limitation_x") == 0)
		{
			Parameters::bound_limitation_x = value;
			DEBUG("Set Parameters::bound_limitation_x to %f", value);
		}
		else if(strcmp(name, "bound_limitation_y") == 0)
		{
			Parameters::bound_limitation_y = value;
			DEBUG("Set Parameters::bound_limitation_y to %f", value);
		}
		
		//Color ranges
		else if(strcmp(name, "color_range_r_min") == 0)
		{
			Parameters::color_range_r_min = value;
			DEBUG("Set Parameters::color_range_r_min to %f", value);
		}
		else if(strcmp(name, "color_range_r_max") == 0)
		{
			Parameters::color_range_r_max = value;
			DEBUG("Set Parameters::color_range_r_max to %f", value);
		}
		else if(strcmp(name, "color_range_g_min") == 0)
		{
			Parameters::color_range_g_min = value;
			DEBUG("Set Parameters::color_range_g_min to %f", value);
		}
		else if(strcmp(name, "color_range_g_max") == 0)
		{
			Parameters::color_range_g_max = value;
			DEBUG("Set Parameters::color_range_g_max to %f", value);
		}
		else if(strcmp(name, "color_range_b_min") == 0)
		{
			Parameters::color_range_b_min = value;
			DEBUG("Set Parameters::color_range_b_min to %f", value);
		}
		else if(strcmp(name, "color_range_b_max") == 0)
		{
			Parameters::color_range_b_max = value;
			DEBUG("Set Parameters::color_range_b_max to %f", value);
		}
		else if(strcmp(name, "color_range_a_min") == 0)
		{
			Parameters::color_range_a_min = value;
			DEBUG("Set Parameters::color_range_a_min to %f", value);
		}
		else if(strcmp(name, "color_range_a_max") == 0)
		{
			Parameters::color_range_a_max = value;
			DEBUG("Set Parameters::color_range_a_max to %f", value);
		}
		
		//Various optimization options
		else if(strcmp(name, "color_hint") == 0)
		{
			Parameters::color_hint = (value > 0.01);
			DEBUG("Set Parameters::color_hint to %s", Parameters::color_hint? "TRUE" : "FALSE");
		}
        else if(strcmp(name, "diff_grid_x") == 0)
		{
			Parameters::diff_grid_x = (cl_uint)value;
			DEBUG("Set Parameters::diff_grid_x to %u", Parameters::diff_grid_x);
		}
		else if(strcmp(name, "diff_grid_y") == 0)
		{
			Parameters::diff_grid_y = (cl_uint)value;
			DEBUG("Set Parameters::diff_grid_y to %u", Parameters::diff_grid_y);
		}
		else if(strcmp(name, "diff_map_gen_count") == 0)
		{
			Parameters::diff_map_gen_count = (cl_uint)value;
			DEBUG("Set Parameters::diff_map_gen_count to %u", Parameters::diff_map_gen_count);
		}
		
		//Embedded Webserver options
		else if(strcmp(name, "embedded_webserver_enabled") == 0)
		{
			Parameters::webserver_enabled = (value > 0.01);
			DEBUG("Set Parameters::webserver_enabled to %s", Parameters::color_hint? "TRUE" : "FALSE");
		}
        
        else if(strcmp(name, "embedded_webserver_port") == 0)
		{
			Parameters::webserver_port = (cl_int)value;
			DEBUG("Set Parameters::webserver_port to %i", Parameters::webserver_port);
		}
		
		//redistribution options
		else if(strcmp(name, "redistribution_threshold") == 0)
		{
			Parameters::redistribution_threshold = value;
			DEBUG("Set Parameters::redistribution_threshold to %f", Parameters::redistribution_threshold);
		}
		
		//DEFAULT
		else
		{
			FAULT("Invalid setting '%s' on line %u", name, line_no);
			return CL_FALSE;
		}
	}
	
	return CL_TRUE;
}
