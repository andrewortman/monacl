#include "helpers/embedded_webserver.hpp"

bool EmbeddedWebServer::handleEvent(ServerHandlingEvent eventCode, MongooseConnection &connection, const MongooseRequest &request, MongooseResponse &response)
{
	if(request.getUri() == std::string("/status"))
    {
        
        std::stringstream ss (std::stringstream::out);
        ss << "{\"generations\": [";
        std::vector<std::string>::iterator itr;
	
	int start = 0; 
	
	if(generationStats.size() > 100) start =  generationStats.size()-100;

        for ( itr = generationStats.begin()+start; itr < generationStats.end(); ++itr )
        {
            ss << *itr;
            if(itr+1 != generationStats.end())
            {
                ss << ", ";
            }
        }
        
        ss << "]}";
        
        response.setStatus(200);
        response.setConnectionAlive(false);
        response.setCacheDisabled();
        response.setContentType("text/html");
        response.addContent(ss.str());
        response.write();
        
        return true;
    }
    
    return false;
}

void EmbeddedWebServer::addGenerationToStats(cl_uint gen_num, cl_float best_fitness, cl_uint total_polygons)
{
    std::stringstream ss (std::stringstream::out);
    ss << "{\"gen_num\": " << gen_num << ", \"best_fitness\": " << (cl_uint)best_fitness << ", \"total_polygons\": " << total_polygons << "}";
    generationStats.push_back(ss.str());
}
