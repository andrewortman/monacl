#ifndef __MONACL_PARAMETERS_HPP__
#define __MONACL_PARAMETERS_HPP__

#include "global.hpp"

class Parameters
{
public:
	static cl_float color_change_mutation_rate;
	static cl_float color_nudge_mutation_rate;
	static cl_float color_nudge_max_amount;
	
	static cl_float alpha_change_mutation_rate;
	static cl_float alpha_nudge_mutation_rate;
	static cl_float alpha_nudge_max_amount;
	
	static cl_float vertex_nudge_far_mutation_rate;
	static cl_float vertex_nudge_far_max_amount;
	static cl_float vertex_nudge_medium_mutation_rate;
	static cl_float vertex_nudge_medium_max_amount;
	static cl_float vertex_nudge_close_mutation_rate;
	static cl_float vertex_nudge_close_max_amount;
	
	static cl_float polygon_swap_layer_rate;
	static cl_float polygon_add_rate;
	static cl_float polygon_remove_rate;
	
	static cl_int population_size;
	static cl_uint start_polygon_count;
	static cl_float selection_percentage;
	static cl_float elitism_percentage;
	
	static cl_float size_limitation_x;
	static cl_float size_limitation_y;
	static cl_float bound_limitation_x;
	static cl_float bound_limitation_y;
	
	static cl_float color_range_r_min;
	static cl_float color_range_r_max;
	static cl_float color_range_g_min;
	static cl_float color_range_g_max;
	static cl_float color_range_b_min;
	static cl_float color_range_b_max;
	static cl_float color_range_a_min;
	static cl_float color_range_a_max;
	
	static cl_bool color_hint;
	static cl_uint diff_grid_x, diff_grid_y;
	static cl_uint diff_map_gen_count;
	
    static cl_bool webserver_enabled;
    static cl_int webserver_port;
    
	static cl_uint image_width, image_height; //this is not to be set using loadFromFile. 
	
	static cl_float redistribution_threshold;
	
	static cl_bool loadFromFile(const char* filename);
};

#endif
