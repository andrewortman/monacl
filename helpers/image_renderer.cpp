#include "helpers/image_renderer.hpp"

cl_bool ImageRenderer::saveReconstructionToPNG(Reconstruction recon, string path)
{
	vector<rgba_t> rendered_image = ComputeManager::renderImage(recon);
	unsigned error =  LodePNG_encode32_file(path.c_str(), (unsigned char*)(&rendered_image[0]), Parameters::image_width, Parameters::image_height);
	return !error;
}

cl_bool ImageRenderer::saveReconstructionToSVG(Reconstruction recon, string path)
{
	stringstream buffer;
	buffer << "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"" << Parameters::image_width 
	<< "\" height=\"" << Parameters::image_height << "\">" << endl; 
	
	for(cl_uint i = 0; i < recon.num_polygons; i++)
	{
		buffer << "\t<polygon points=\"";
		for(cl_uint g = 0; g < NUM_VERTICES; g++)
		{
			buffer << (cl_int)(recon.polygons[i].vertices[g].x*Parameters::image_width) << "," << (cl_int)(recon.polygons[i].vertices[g].y*Parameters::image_height);
			if(g != NUM_VERTICES-1)
			{
				buffer << " ";
			}
		}
		
		buffer << "\" style=\"fill:rgb(" << (int)recon.polygons[i].color.r << "," <<
		 		(int)recon.polygons[i].color.g << "," << (int)recon.polygons[i].color.b << ");\"";
		
		buffer << " opacity = \"" << (float)(recon.polygons[i].color.a/255.0f) << "\" />" << endl;
	}
	
	buffer << "</svg>";
	
	ofstream output_stream(path.c_str(), ios::out);
	if(output_stream.fail()) return CL_FALSE;
	output_stream << buffer.str();
	output_stream.close();
	if(output_stream.fail()) return CL_FALSE;
	else return CL_TRUE;
}
