#include "data/polygon.hpp"

static cl_bool checkPolygonLimits(Polygon *p)
{
	boundingbox_t bb = p->getBoundingBox();
	
	cl_float size_width = bb.max.x - bb.min.x;
	cl_float size_height = bb.max.y - bb.min.y;
	
	if(size_width > Parameters::size_limitation_x)
	{
		//DEBUG("Size x failed");
		return CL_FALSE;
	}
	if(size_height > Parameters::size_limitation_y)
	{
	//	DEBUG("Size y failed");
		return CL_FALSE;
	}
	
	/* Bound check */
	if(bb.min.x < (1-Parameters::bound_limitation_x)) 
	{
	//	DEBUG("Bound min x failed");
		return CL_FALSE;
	}
	if(bb.max.x > Parameters::bound_limitation_x)
	{
	//	DEBUG("Bound max x failed");
		return CL_FALSE;
	}
	
	if(bb.min.y < (1-Parameters::bound_limitation_y))
	{
	//	DEBUG("Bound min y failed");
		return CL_FALSE;
	}
	
	if(bb.max.y > (Parameters::bound_limitation_y))
	{
	//	DEBUG("Bound max y failed");
		return CL_FALSE;
	}
	
	return CL_TRUE;
}

boundingbox_t Polygon::getBoundingBox()
{
	boundingbox_t bb;
	bb.min.x = this->vertices[0].x;
	bb.min.y = this->vertices[0].y;
	bb.max.x = this->vertices[0].x;
	bb.max.y = this->vertices[0].y;
	
	for(cl_int i = 0; i < NUM_VERTICES; i++)
	{
		if(this->vertices[i].x < bb.min.x)
			bb.min.x = this->vertices[i].x;
		
		if(this->vertices[i].y < bb.min.y)
			bb.min.y = this->vertices[i].y;
			
		if(this->vertices[i].x > bb.max.x)
			bb.max.x = this->vertices[i].x;
			
		if(this->vertices[i].y > bb.max.y)
			bb.max.y = this->vertices[i].y;
	}
	return bb;
}

Polygon::Polygon(vector<vertex_t> vertices, rgba_t color)
{
	if(vertices.size() != NUM_VERTICES)	FAULT("Vertex count of each polygon must be %i", NUM_VERTICES);
	this->color = color;
	std::copy(vertices.begin(), vertices.end(), this->vertices);
}

Polygon Polygon::RandomPolygon(unsigned int &r_seed)
{
	vector<vertex_t> vertices;
	vertices.resize(NUM_VERTICES);
	vertices[0].x = RAND_RF_RANGE(r_seed, (cl_float)(1-Parameters::bound_limitation_x), (cl_float)Parameters::bound_limitation_x);
	vertices[0].y = RAND_RF_RANGE(r_seed, (cl_float)(1-Parameters::bound_limitation_y), (cl_float)Parameters::bound_limitation_y);
	
	for(unsigned int i = 1; i < NUM_VERTICES; i++)
	{
		cl_float max_x = (vertices[0].x + Parameters::size_limitation_x);
		cl_float max_y = (vertices[0].y + Parameters::size_limitation_y);
		
		if(max_x > Parameters::bound_limitation_x) max_x = Parameters::bound_limitation_x;
		if(max_y > Parameters::bound_limitation_y) max_y = Parameters::bound_limitation_y;
		
		vertices[i].x = RAND_RF_RANGE(r_seed, vertices[0].x, max_x);
		vertices[i].y = RAND_RF_RANGE(r_seed, vertices[0].y, max_y);
	}
	
	rgba_t color;
	color.r = RAND_RI_RANGE(r_seed, (cl_uint)(256 * Parameters::color_range_r_min), (cl_uint)(256 * Parameters::color_range_r_max));
	color.g = RAND_RI_RANGE(r_seed, (cl_uint)(256 * Parameters::color_range_g_min), (cl_uint)(256 * Parameters::color_range_g_max));
	color.b = RAND_RI_RANGE(r_seed, (cl_uint)(256 * Parameters::color_range_b_min), (cl_uint)(256 * Parameters::color_range_b_max));
	color.a = RAND_RI_RANGE(r_seed, (cl_uint)(256 * Parameters::color_range_a_min), (cl_uint)(256 * Parameters::color_range_a_max));
	return Polygon(vertices, color);
}

Polygon Polygon::RandomPolygon(unsigned int &r_seed, vector<float> diff_map)
{
	//first find sum of diff map
	cl_float diff_sum = 0.0f;
	cl_uint i;
	for(i = 0; i < diff_map.size(); i++) diff_sum += diff_map[i];
	
	//now, pick a number between 0 and diffsum
	cl_float rand_point = RAND_RF_RANGE(r_seed, 0, diff_sum);
	diff_sum = 0.0f;
	for(i = 0; i< diff_map.size(); i++)
	{
		diff_sum += diff_map[i];
		if(diff_sum > rand_point)
		{
			break;
		}
	}
	
	//now go through and determine where that point is in the map
	cl_float center_x = ((.5f + (i % Parameters::diff_grid_x)) / (float)Parameters::diff_grid_x);
	cl_float center_y = ((.5f + (i / Parameters::diff_grid_x)) / (float)Parameters::diff_grid_y);
	
	DEBUG("DM algorithm placing polygon near (%f,%f)", center_x, center_y);
	
	vector<vertex_t> vertices;
	vertices.resize(NUM_VERTICES);
	
	//Now add vertices around this center point up to half of the max away
	for(unsigned int i = 0; i < NUM_VERTICES; i++)
	{
		cl_float max_x = (center_x + Parameters::size_limitation_x/2.0f);
		cl_float max_y = (center_y + Parameters::size_limitation_y/2.0f);
		
		if(max_x > Parameters::bound_limitation_x) max_x = Parameters::bound_limitation_x;
		if(max_y > Parameters::bound_limitation_y) max_y = Parameters::bound_limitation_y;
		
		cl_float min_x = (center_x - Parameters::size_limitation_x/2.0f);
		cl_float min_y = (center_y - Parameters::size_limitation_y/2.0f);
		
		if(min_x < (1-Parameters::bound_limitation_x)) min_x = 1-Parameters::bound_limitation_x;
		if(min_y < (1-Parameters::bound_limitation_y)) min_y = 1-Parameters::bound_limitation_y;
		
		vertices[i].x = RAND_RF_RANGE(r_seed, min_x, max_x);
		vertices[i].y = RAND_RF_RANGE(r_seed, min_y, max_y);
	}
	
	//Random color for now
	rgba_t color;
	color.r = RAND_RI_RANGE(r_seed, (cl_uint)(256 * Parameters::color_range_r_min), (cl_uint)(256 * Parameters::color_range_r_max));
	color.g = RAND_RI_RANGE(r_seed, (cl_uint)(256 * Parameters::color_range_g_min), (cl_uint)(256 * Parameters::color_range_g_max));
	color.b = RAND_RI_RANGE(r_seed, (cl_uint)(256 * Parameters::color_range_b_min), (cl_uint)(256 * Parameters::color_range_b_max));
	color.a = RAND_RI_RANGE(r_seed, (cl_uint)(256 * Parameters::color_range_a_min), (cl_uint)(256 * Parameters::color_range_a_max));
	return Polygon(vertices, color);
}

cl_uint Polygon::mutate(unsigned int &r_seed)
{
	cl_uint num_mutations = 0;
	
	if(RAND_RB_CHANCE(r_seed, Parameters::color_change_mutation_rate))
	{
		//change the color completely
		this->color.r = RAND_RI_RANGE(r_seed,(cl_int)(255*Parameters::color_range_r_min), (cl_int)(255*Parameters::color_range_r_max));
		this->color.g = RAND_RI_RANGE(r_seed,(cl_int)(255*Parameters::color_range_g_min), (cl_int)(255*Parameters::color_range_g_max));
		this->color.b = RAND_RI_RANGE(r_seed,(cl_int)(255*Parameters::color_range_b_min), (cl_int)(255*Parameters::color_range_b_max));
		
		num_mutations++;
	}
	
	if(RAND_RB_CHANCE(r_seed, Parameters::alpha_change_mutation_rate))
	{
		//change the alpha completely
		this->color.a = RAND_RI_RANGE(r_seed,(cl_int)(255*Parameters::color_range_a_min), (cl_int)(255*Parameters::color_range_a_max));
		num_mutations++;
	}
	
	if(RAND_RB_CHANCE(r_seed, Parameters::color_nudge_mutation_rate))
	{
		
		cl_int min_r = (cl_int)(255*Parameters::color_range_r_min);
		cl_int max_r = (cl_int)(255*Parameters::color_range_r_max);
		cl_int min_g = (cl_int)(255*Parameters::color_range_g_min);
		cl_int max_g = (cl_int)(255*Parameters::color_range_g_max);
		cl_int min_b = (cl_int)(255*Parameters::color_range_b_min);
		cl_int max_b = (cl_int)(255*Parameters::color_range_b_max);
		
		cl_int nudge_max_amount_r = (cl_int)(Parameters::color_nudge_max_amount * (max_r - min_r));
		cl_int nudge_max_amount_g = (cl_int)(Parameters::color_nudge_max_amount * (max_g - min_g));
		cl_int nudge_max_amount_b = (cl_int)(Parameters::color_nudge_max_amount * (max_b - min_b));
		
		if(nudge_max_amount_r == 0)
		{
			FAULT("Color nudge max red amount equivilant to 0! (must be at least %f)", 1/255.0f);
		}
		if(nudge_max_amount_g == 0)
		{
			FAULT("Color nudge max green amount equivilant to 0! (must be at least %f)", 1/255.0f);
		}
		if(nudge_max_amount_b == 0)
		{
			FAULT("Color nudge max blue amount equivilant to 0! (must be at least %f)", 1/255.0f);
		}
		
		cl_int amount_r = RAND_RI_RANGE(r_seed, -nudge_max_amount_r, nudge_max_amount_r);
		cl_int amount_g = RAND_RI_RANGE(r_seed, -nudge_max_amount_g, nudge_max_amount_g);
		cl_int amount_b = RAND_RI_RANGE(r_seed, -nudge_max_amount_b, nudge_max_amount_b);
		
		//nudge r
		cl_int new_r = (cl_int)this->color.r + amount_r;
		if(new_r < min_r) new_r = min_r;
		if(new_r > max_r) new_r = max_r;
		this->color.r = (cl_uchar)new_r;
		
		//nudge g
		cl_int new_g = (cl_int)this->color.g + amount_g;
		if(new_g < min_g) new_g = min_g;
		if(new_g > max_g) new_g = max_g;
		this->color.g = (cl_uchar)new_g;
		
		//nudge b
		cl_int new_b = (cl_int)this->color.b + amount_b;
		if(new_b < min_b) new_b = min_b;
		if(new_b > max_b) new_b = max_b;
		this->color.b = (cl_uchar)new_b;
		
		num_mutations++;
	}
	
	if(RAND_RB_CHANCE(r_seed, Parameters::alpha_nudge_mutation_rate))
	{	
		cl_int min_a = (cl_int)(255*Parameters::color_range_a_min);
		cl_int max_a = (cl_int)(255*Parameters::color_range_a_max);
		
		cl_int nudge_max_amount = (cl_int)(Parameters::alpha_nudge_max_amount * (max_a - min_a));
		if(nudge_max_amount == 0)
		{
			FAULT("Alpha nudge max amount equivilant to 0! (must be at least %f)", 1/255.0f);
		}
		
		cl_int amount_a = RAND_RI_RANGE(r_seed, -nudge_max_amount, nudge_max_amount);
		
		//nudge a
		cl_int new_a = (cl_int)this->color.a + amount_a;
		if(new_a < min_a) new_a = min_a;
		if(new_a > max_a) new_a = max_a;
		this->color.a = (cl_uchar)new_a;
		
		num_mutations++;
	}
	
	
	/* VECTOR POSITIONING */
	for(int i = 0; i < NUM_VERTICES; i++)
	{
		if(RAND_RB_CHANCE(r_seed, Parameters::vertex_nudge_far_mutation_rate))
		{	
			cl_float original_x = vertices[i].x;
			cl_float original_y = vertices[i].y;
			do
			{
				cl_float vertex_amount_x = RAND_RF_RANGE(r_seed, -Parameters::vertex_nudge_far_max_amount, Parameters::vertex_nudge_far_max_amount);
				cl_float vertex_amount_y = RAND_RF_RANGE(r_seed, -Parameters::vertex_nudge_far_max_amount, Parameters::vertex_nudge_far_max_amount);
			
				vertices[i].x = original_x + vertex_amount_x;
				vertices[i].y = original_y + vertex_amount_y;
			} while(!checkPolygonLimits(this));
			
			num_mutations++;
			
		}
		
		if(RAND_RB_CHANCE(r_seed, Parameters::vertex_nudge_medium_mutation_rate))
		{
			cl_float original_x = vertices[i].x;
			cl_float original_y = vertices[i].y;
			do
			{
				cl_float vertex_amount_x = RAND_RF_RANGE(r_seed, -Parameters::vertex_nudge_medium_max_amount, Parameters::vertex_nudge_medium_max_amount);
				cl_float vertex_amount_y = RAND_RF_RANGE(r_seed, -Parameters::vertex_nudge_medium_max_amount, Parameters::vertex_nudge_medium_max_amount);
			
				vertices[i].x = original_x + vertex_amount_x;
				vertices[i].y = original_y + vertex_amount_y;
			} while(!checkPolygonLimits(this));
			
			num_mutations++;
			
		}
		
		if(RAND_RB_CHANCE(r_seed, Parameters::vertex_nudge_close_mutation_rate))
		{			
			cl_float original_x = vertices[i].x;
			cl_float original_y = vertices[i].y;
			do
			{
				cl_float vertex_amount_x = RAND_RF_RANGE(r_seed, -Parameters::vertex_nudge_close_max_amount, Parameters::vertex_nudge_close_max_amount);
				cl_float vertex_amount_y = RAND_RF_RANGE(r_seed, -Parameters::vertex_nudge_close_max_amount, Parameters::vertex_nudge_close_max_amount);
			
				vertices[i].x = original_x + vertex_amount_x;
				vertices[i].y = original_y + vertex_amount_y;
			} while(!checkPolygonLimits(this));
			
			num_mutations++;
			
		}
	}
	
	return num_mutations;
}
