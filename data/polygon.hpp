#ifndef __MONACL_POLYGON_HPP__
#define __MONACL_POLYGON_HPP__

#include "global.hpp"
#include "data/data_structs.hpp"
#include "helpers/parameters.hpp"

typedef struct
{
	vertex_t min, max;
} boundingbox_t;

class Polygon : public polygon_t
{
public:
	
	/**
	 * Creates a polygon object with a vector of vertices and a color
	 * \brief Primary constructor for a Polygon
	 * \param vertices a vector of vertexes (vertex_t) that make up a polygon
	 * \param color the color (and alpha) of a polygon (rgba_t)
	 */
	Polygon(vector<vertex_t> vertices, rgba_t color);
	
	/**
	 * \brief Returns the bounding box of the polygon
	 */
	boundingbox_t getBoundingBox();
	
	/**
	 * \brief Returns a completely random polygon within the width and height
	 *
	 * \param r_seed The reentrant random seed modified by rand_r
	 */
	static Polygon RandomPolygon(unsigned int &r_seed);
	
	/**
	 * \brief Returns a random polygon placed at a location with a bias where the detail is lacking.
	 * \param r_seed The reentrant random seed modified by rand_r
	 * \param diff_map The standard difference map created by compute manager
	 */
	static Polygon RandomPolygon(unsigned int &r_seed, vector<float> diff_map);
	
	/**
	 * \brief Mutates the polygon
	 * 
	 * Mutates the polygon based of the rates defined in parameters.hpp
	 * \param r_seed the reentrant seed for rand_r
	 * \return the number of mutations completed
	 */
	cl_uint mutate(unsigned int &r_seed);
};

#endif
