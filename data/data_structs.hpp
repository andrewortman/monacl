#ifndef __MONACL_DATA_STRUCTS__
#define __MONACL_DATA_STRUCTS__

#include "global.hpp"

/* BEGIN OPENCL DATA STRUCTS */
/* THESE MUST NOT BE MODIFIED WITHOUT MODIFYING THE STRUCTS IN THE OPENCL KERNEL TOO! */

typedef struct  {
	cl_uchar r,g,b,a; //32 bits
} rgba_t;

typedef struct  {
	cl_float x, y;
} vertex_t;

typedef struct {
	vertex_t	vertices[NUM_VERTICES]; //32 bit each
	rgba_t		color;	
} polygon_t;

typedef struct {
	polygon_t polygons[MAX_POLYGONS]; //32*NUM_VERT + 32 bit
	cl_uint  num_polygons; //32 bits
	cl_float fitness;	//most recent fitness calculation
	cl_bool validated;  //The fitness corresponds with the current state of the reconstruction
} reconstruction_t;

/* END OPENCL DATA STRUCTS */

#endif
