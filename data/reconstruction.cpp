#include "data/reconstruction.hpp"

Reconstruction::Reconstruction()
{
	this->num_polygons = 0;
}

Reconstruction::Reconstruction(vector<Polygon> polygons)
{
	if(polygons.size() > MAX_POLYGONS)	FAULT("Polygon count exceeds maximum!");
	this->num_polygons = polygons.size();
	std::copy(polygons.begin(), polygons.end(), this->polygons);
}

void Reconstruction::addPolygon(Polygon polygon)
{
	if(this->num_polygons == MAX_POLYGONS) FAULT("Cannot add polygon to reconstruction - would exceed maximum");
	this->polygons[this->num_polygons++] = polygon;
}

void Reconstruction::removePolygon(cl_uint idx)
{
	if(idx >= this->num_polygons) FAULT("Cannot remove polygon to reconstruction - invalid index");
	std::copy(polygons+idx+1, polygons+this->num_polygons, polygons+idx); //shift idx+1 and on left
	this->num_polygons--;
}

cl_uint Reconstruction::mutate(unsigned int &r_seed, vector<float> diff_map)
{
	cl_uint mutations = 0;
	for(unsigned int i = 0; i < num_polygons; i++)
	{
		//because polygon's are stored as 'polygon_t' in memory, we have to 
		//reinterpret the data as a "Polygon" instead.
		mutations += (static_cast<Polygon*>(&polygons[i]))->mutate(r_seed);
	}
	
	if(RAND_RB_CHANCE(r_seed, Parameters::polygon_swap_layer_rate) && this->num_polygons > 2)
	{
		unsigned int index_a = RAND_RI_RANGE(r_seed, 0, this->num_polygons);
		unsigned int index_b = 0;
		do
		{
			index_b = RAND_RI_RANGE(r_seed, 0, this->num_polygons);
		}
		while(index_a == index_b);
		
		swap(this->polygons[index_a], this->polygons[index_b]);
		mutations++;
	}
	
	if(RAND_RB_CHANCE(r_seed, Parameters::polygon_remove_rate))
	{
		if(this->num_polygons > 0)
		{
			this->removePolygon(RAND_RI_RANGE(r_seed, 0, this->num_polygons));
			mutations++;
		}
	}
	
	if(RAND_RB_CHANCE(r_seed, Parameters::polygon_add_rate))
	{
		if(this->num_polygons < MAX_POLYGONS)
		{
			if(diff_map.size() != 0)
			{
				Polygon p = Polygon::RandomPolygon(r_seed, diff_map);
				this->addPolygon(p);
			}
			else
			{
				Polygon p = Polygon::RandomPolygon(r_seed);
				this->addPolygon(p);
			}
			
			mutations++;
		}
	}
	return mutations;
}

bool Reconstruction::operator<(const Reconstruction &right) const
{
	return this->fitness < right.fitness;
}

void Reconstruction::invalidateAll(vector<Reconstruction> &pool)
{
	for(unsigned int i = 0; i < pool.size(); i++)
	{
		pool[i].invalidate();
	}
	DEBUG("Invalidated all members of pool");
}

cl_bool Reconstruction::isValidatedAll(vector<Reconstruction> &pool)
{
	for(unsigned int i = 0; i < pool.size(); i++)
	{
		if(!pool[i].isValidated()) return CL_FALSE;
	}
	
	return CL_TRUE;
}

MPI_Datatype Reconstruction::GetMPIType()
{
	MPI_Datatype rgba_type, vertex_type, polygon_type, recon_type;
	
	//First, build the rgba type
	MPI_Aint rgba_block_displacements[] =  {offsetof(rgba_t, r),
											offsetof(rgba_t, g),
											offsetof(rgba_t, b),
											offsetof(rgba_t, a)};
	int rgba_block_lengths[] = 		   	   {1,1,1,1};
	MPI_Datatype rgba_types[] = 		   {MPI_UNSIGNED_CHAR, 
											MPI_UNSIGNED_CHAR, 
											MPI_UNSIGNED_CHAR,
											MPI_UNSIGNED_CHAR};
							
	HANDLE_MPI_ERROR(MPI_Type_create_struct(4, rgba_block_lengths, rgba_block_displacements, 
											rgba_types, &rgba_type));
	HANDLE_MPI_ERROR(MPI_Type_commit(&rgba_type));
	
	int rgba_size = 0;
	MPI_Type_size(rgba_type, &rgba_size);
	DEBUG("MPI_SIZE for RGBA: %i. TYPEOF: %i", rgba_size, (int)sizeof(rgba_t));
	
	
	//Next, build the vertex type
	MPI_Aint vertex_block_displacements[] = {offsetof(vertex_t, x),
											 offsetof(vertex_t, y)};
	int vertex_block_lengths[] = 		   {1,1};
	MPI_Datatype vertex_types[] = 		   {MPI_FLOAT,
											MPI_FLOAT};
							
	HANDLE_MPI_ERROR(MPI_Type_create_struct(2, vertex_block_lengths, vertex_block_displacements,
											vertex_types, &vertex_type));
	HANDLE_MPI_ERROR(MPI_Type_commit(&vertex_type));
	
	int vertex_size = 0;
	MPI_Type_size(vertex_type, &vertex_size);
	DEBUG("MPI_SIZE for VERTEX: %i. TYPEOF: %i", vertex_size, (int)sizeof(vertex_t));
	
	//Next build the polygon type
	MPI_Aint pg_block_displacements[] = 	{offsetof(polygon_t, vertices),
											 offsetof(polygon_t, color)};
	int pg_block_lengths[] = 		        {NUM_VERTICES,1};
	MPI_Datatype pg_types[] = 		        {vertex_type,
											rgba_type};
							
	HANDLE_MPI_ERROR(MPI_Type_create_struct(2, pg_block_lengths, pg_block_displacements,
											pg_types, &polygon_type));
	HANDLE_MPI_ERROR(MPI_Type_commit(&polygon_type));
	
	int pg_size = 0;
	MPI_Type_size(polygon_type, &pg_size);
	DEBUG("MPI_SIZE for POLYGON: %i. TYPEOF: %i", pg_size, (int)sizeof(polygon_t));
	
	
	//Finally, build the reconstruction type
	MPI_Aint recon_block_displacements[] = 	{offsetof(reconstruction_t, polygons),
											offsetof(reconstruction_t, num_polygons),
											offsetof(reconstruction_t, fitness),
											offsetof(reconstruction_t, validated)};
	int recon_block_lengths[] = 		    {MAX_POLYGONS,1,1,1};
	MPI_Datatype recon_types[] = 		    {polygon_type,
											MPI_UNSIGNED,
											MPI_FLOAT,
											MPI_UNSIGNED};
							
	HANDLE_MPI_ERROR(MPI_Type_create_struct(4, recon_block_lengths, recon_block_displacements,
	 						recon_types, &recon_type));
	HANDLE_MPI_ERROR(MPI_Type_commit(&recon_type));
	
	int recon_size = 0;
	MPI_Type_size(recon_type, &recon_size);
	DEBUG("MPI_SIZE for RECON: %i. TYPEOF: %i", recon_size, (int)sizeof(reconstruction_t));
	
	
	return recon_type;
}



