#ifndef __MONACL_RECONSTRUCTION_HPP__
#define __MONACL_RECONSTRUCTION_HPP__

#include "global.hpp"
#include "data/data_structs.hpp"
#include "data/polygon.hpp"
#include "helpers/lodepng.hpp"
#include "mpi.h"

class Reconstruction : public reconstruction_t
{
public:
	
	/**
	 * @brief Default constructor for a reconstruction image
	 * 
	 * Initializes an empty reconstruction (0 polygons)
	 */
	Reconstruction();
	
	/**
	 * @brief Primary constructor for a reconstruction image
	 * 
	 * Takes in a vector of Polygons and stores that into the base struct (reconstruction_t)
	 * @param polygons Vector of Polygon objects
	 */
	Reconstruction(vector<Polygon> polygons);
	
	/**
	 * @brief Add a polygon to the reconstruction group. 
	 * 
	 * Note: Make sure that the group has enough space!
	 *
	 * @param polygon The new polygon to add
	 */
	void addPolygon(Polygon polygon);
	
	/**
	 * @brief Removes a polygon to the reconstruction group. 
	 * 
	 * Note: Make sure that the group is not empty
	 *
	 * @param polygon The index of the element to remove
	 */
	void removePolygon(cl_uint idx);
	
	/**
	 * @brief Mutates the reconstruction
	 * 
	 * Mutates the reconstruction based of the rates defined in parameters.hpp
	 *
	 * @param r_seed the reentrant seed for rand_r
	 * @param diff_map the difference map used for adding polygons in appropriate places
	 * @return the number of mutations completed
	 */
	cl_uint mutate(unsigned int &r_seed, vector<float> diff_map);
	
	
	/**
	 * @brief invalidates the current reconstruction
	 */
	void invalidate() { this->validated = false; }
	
	/**
	 * @brief validates the current reconstruction
	 */
	void validate() { this->validated = true; }
	
	/**
	 * @brief Checks to see if the reconstruction is validated or not 
	 */
	cl_bool isValidated() { return this->validated; }
	
	/**
	 * @brief Sets the fitness for the reconstruction
	 * @param new_fitness the new fitness
	 */
	void setFitness(cl_float new_fitness) { this->fitness = new_fitness; }
	
	/**
	 * @brief Gets the current fitness for the reconstruction. This is only valid when isValidated returns true 
	 * @return the reconstruction's calculated fitness value 
	 */
	cl_float getFitness() { return this->fitness; }
	
	/**
	 * @brief Used for sorting. 
	 * @param right the right side of the comparison
	 * @return Returns true if this reconstruction has a smaller fitness than the right argument
	 */
	bool operator<(const Reconstruction &right) const;
	
	/** 
	 * @brief Checks an entire vector of reconstructions for validity
	 * @param pool The reconstruction pool to check
	 * @return If any member of the vector is not valid, this function returns false
	 */
	static cl_bool isValidatedAll(vector<Reconstruction> &pool);
	
	/**
	 * @brief Invalidates an entire vector of reconstrucion
	 * @param pool The pool of reconstructions to invalidate
	 */
	static void invalidateAll(vector<Reconstruction> &pool);
	
	/**
	 * @brief Constructs an MPI Datatype for the reconstruction structure
	 */
	static MPI_Datatype GetMPIType();

};

#endif
