#include "communications/distribution_manager.hpp"

vector<DistributionElement> DistributionManager::distribution_info;

DistributionElement::DistributionElement(int node_id, int initial_population_size)
{
	this->node_id = node_id;
	this->population_size = this->population_size_target = initial_population_size;
}

bool DistributionElement::operator<(const DistributionElement &right) const
{
	int abs_offset_left = this->getOffset();
	int abs_offset_right = right.getOffset();
	
	abs_offset_left = abs_offset_left < 0 ? -abs_offset_left : abs_offset_left;
	abs_offset_right = abs_offset_right < 0 ? -abs_offset_right : abs_offset_right;
	
	return abs_offset_left < abs_offset_right;
}

void DistributionManager::Initialize()
{
	int num_nodes = MPIManager::GetCommSize();
	int population_size = Parameters::population_size;
	if(population_size % num_nodes != 0)
	{
		Parameters::population_size -= population_size%num_nodes;
		INFO("WARNING: Population size not divisible by node count. Real size = %i", Parameters::population_size);
	}
	population_size = Parameters::population_size;
	
	int size_per_node = population_size / num_nodes;
	
	distribution_info.reserve(num_nodes);
	for(int i = 0; i < num_nodes; i++)
	{
		DistributionElement de(i, size_per_node);
		distribution_info.push_back(de);
	}
	
	DEBUG("DistributionManager Initialized!");
}

void DistributionManager::HandleRedistribution(vector<ResponseHeaders> &response_headers)
{
	//first get a vector of possible senders and receivers
	vector<DistributionElement> possible_senders, possible_receivers;
	for(unsigned i = 0; i < response_headers.size(); i++)
	{
		int new_size = response_headers[i].getNumberOfCommands();
		distribution_info[i].setPopulationSize(new_size);
		
		
		//determine if the threshold is reached for sending or receiving
		int abs_offset = distribution_info[i].getOffset();
		abs_offset = abs_offset < 0 ? -abs_offset : abs_offset;
		
		int threshold = Parameters::redistribution_threshold*distribution_info[i].getTarget();
		if(abs_offset < threshold) continue;
		
		//the threshold was reached here, add it to the appropriate list
		if(distribution_info[i].getOffset() < 0) possible_receivers.push_back(distribution_info[i]);
		else possible_senders.push_back(distribution_info[i]);
	}	
	
	//counts of possible (receivers|senders)
	unsigned pr_cnt = possible_receivers.size();
	unsigned ps_cnt = possible_senders.size();
	
	INFO("RM: %u possible senders, %u possible receivers", ps_cnt, pr_cnt);
	
	//find minimum of each list
	unsigned max_transfers = (ps_cnt < pr_cnt ? ps_cnt : pr_cnt);
	
	//Now mark the response_headers for the appropriate transfers and also update the set counts
	for(unsigned i = 0; i < max_transfers; i++)
	{
		DistributionElement sender = possible_senders[i];
		DistributionElement receiver = possible_receivers[i];
		
		int amount = sender.getOffset() - receiver.getOffset();
		amount /= 2;
		
		DEBUG("TRANSFER from %i to %i (size %i)", sender.getRank(), receiver.getRank(), amount);
		
		response_headers[sender.getRank()].setIsToSendRedistribute(receiver.getRank(), amount);
		response_headers[receiver.getRank()].setIsToRecvRedistribute(sender.getRank(), amount);
		
		//update the local 
		distribution_info[sender.getRank()].setPopulationSize(sender.getPopulationSize()-amount);
		distribution_info[receiver.getRank()].setPopulationSize(receiver.getPopulationSize()+amount);
	}
}

vector<unsigned> DistributionManager::GetLocalSetSizes()
{
	vector<unsigned> set_sizes;
	set_sizes.resize(distribution_info.size());
	
	for(int i = 0; i < distribution_info.size(); i++)
	{
		set_sizes[distribution_info[i].getRank()] = distribution_info[i].getPopulationSize();
	}
	
	return set_sizes;
}

