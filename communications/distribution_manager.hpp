#ifndef __MONACL_DISTRIBUTION_MANAGER_HPP__
#define __MONACL_DISTRIBUTION_MANAGER_HPP__

#include "global.hpp"
#include "communications/mpi_manager.hpp"
#include "helpers/parameters.hpp"

class DistributionElement
{
	private:
		int node_id;
		unsigned population_size;
		unsigned population_size_target;
		
	public:
		bool operator<(const DistributionElement &right) const;
		DistributionElement(int node_id, int initial_population_size);
		
		int getRank() const { return node_id; }
		int getPopulationSize() const {	return population_size;	}
		int getTarget() const { return population_size_target; }
		int getOffset() const { return population_size - population_size_target; }
		
		void setPopulationSize(int new_size) { 	this->population_size = new_size; }
};
			
class DistributionManager 
{
	private:
		static vector<DistributionElement> distribution_info;
	
	public:
		static void Initialize();
		static void HandleRedistribution(vector<ResponseHeaders> &response_headers);
		static vector<unsigned> GetLocalSetSizes();
};

#endif
