#include "communications/response_headers.hpp"

//Initialize the response header to be empty.
ResponseHeaders::ResponseHeaders()
{
	this->methods = REDISTRIBUTE_NONE & ~SEND_BACK_RECONSTRUCTION;
	this->number_of_elite_commands = 0;
	this->number_of_mutation_commands = 0;
	this->redistribute_partner = 0;
	this->sendback_recon_index = 0;
}

void ResponseHeaders::setIsToSendBackReconstruction(unsigned int sendback_index)
{
	this->methods |= SEND_BACK_RECONSTRUCTION;
	this->sendback_recon_index = sendback_index;
}

void ResponseHeaders::setNumberOfMutationCommands(unsigned int number)
{
	this->number_of_mutation_commands = number;
}

void ResponseHeaders::setNumberOfEliteCommands(unsigned int number)
{
	this->number_of_elite_commands = number;
}

void ResponseHeaders::setIsToSendRedistribute(unsigned int partner_rank, unsigned int count)
{
	this->methods &= ~REDISTRIBUTE_RECV;
	this->methods |= REDISTRIBUTE_SEND;
	this->redistribute_partner = partner_rank;
	this->redistribute_count = count;
}

void ResponseHeaders::setIsToRecvRedistribute(unsigned int partner_rank, unsigned int count)
{
	this->methods &= ~REDISTRIBUTE_SEND;
	this->methods |= REDISTRIBUTE_RECV;
	this->redistribute_partner = partner_rank;
	this->redistribute_count = count;
}

bool ResponseHeaders::isToSendBackReconstruction()
{
	return (this->methods & SEND_BACK_RECONSTRUCTION);
}

bool ResponseHeaders::isToRedistribute()
{
	return this->isToRecvRedistribute() || this->isToSendRedistribute();
}

bool ResponseHeaders::isToSendRedistribute()
{
	return !(this->methods & REDISTRIBUTE_RECV) && (this->methods & REDISTRIBUTE_SEND);
}

bool ResponseHeaders::isToRecvRedistribute()
{
	return !(this->methods & REDISTRIBUTE_SEND) && (this->methods & REDISTRIBUTE_RECV);
}

unsigned int ResponseHeaders::getNumberOfMutationCommands()
{
	return this->number_of_mutation_commands;
}

unsigned int ResponseHeaders::getNumberOfEliteCommands()
{
	return this->number_of_elite_commands;
}

unsigned int ResponseHeaders::getNumberOfCommands()
{
	return this->getNumberOfMutationCommands() + this->getNumberOfEliteCommands();
}

unsigned int ResponseHeaders::getRedistributePartnerRank()
{
	return this->redistribute_partner;
}

unsigned int ResponseHeaders::getRedistributeCount()
{
	return this->redistribute_count;
}


unsigned int ResponseHeaders::getSendBackIndex()
{
	return this->sendback_recon_index;
}

MPI_Datatype ResponseHeaders::GetMPIType()
{
	MPI_Datatype datatype;
	
	//First, build the rgba type
	MPI_Aint displacements[] =  {	offsetof(response_headers_t, methods),
					offsetof(response_headers_t, number_of_mutation_commands),
					offsetof(response_headers_t, number_of_elite_commands),
					offsetof(response_headers_t, redistribute_partner),
					offsetof(response_headers_t, redistribute_count),
					offsetof(response_headers_t, sendback_recon_index)};

	int lengths[] =			{1,1,1,1,1,1};
	MPI_Datatype types[] =		{MPI_CHAR, 
								MPI_UNSIGNED, 
								MPI_UNSIGNED,
								MPI_UNSIGNED,
								MPI_UNSIGNED,
								MPI_UNSIGNED};
							
	HANDLE_MPI_ERROR(MPI_Type_create_struct(6, lengths, displacements, 
						types, &datatype));
	HANDLE_MPI_ERROR(MPI_Type_commit(&datatype));
	return datatype;
}

