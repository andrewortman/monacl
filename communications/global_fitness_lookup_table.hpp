#ifndef __MONACL_GLOBAL_FITNESS_LOOKUP_TABLE_HPP__
#define __MONACL_GLOBAL_FITNESS_LOOKUP_TABLE_HPP__

#include "global.hpp"
#include "communications/mpi_manager.hpp"
#include "helpers/parameters.hpp"

class GlobalFitnessLookupTableElement
{
	private:
		unsigned int rank;
		unsigned int local_index;
		float fitness;
	
	public:
		
		/**
		 * \brief Default initializer for the lookup table elements
		 * \param rank The rank containing the reconstruction
		 * \param index The local index of the reconstruction
		 * \param fitness The fitness result of the reconstrucion
		 */
		GlobalFitnessLookupTableElement(int rank, int index, float fitness);
		
		/**
		 * \brief Returns the rank portion of the element
		 */
		unsigned int getRank() const;
		
		/**
		 * \brief Returns the index portion of the element
		 */
		unsigned int getLocalIndex() const;
		
		/**
		 * \brief Returns the fitness portion of the element
		 */
		float getFitness() const;
		
		/**
		 * \brief Compares fitness between this element and another
		 * \param right Right side of the operation
		 * \return True if my fitness is less than right's fitness
		 */
		bool operator<(const GlobalFitnessLookupTableElement &right) const;
};

class GlobalFitnessLookupTable
{
	private:
		vector<GlobalFitnessLookupTableElement> elements;
		bool is_sorted;
		int ranking_sum;
		
		/**
		 * \brief Sorts the table by fitness (ascending order)
		 * Does not resort if table is already sorted.
		 */
		void sortTableByFitness();

	public:
		/**
		 * \brief Creates a empty table
		 */
		GlobalFitnessLookupTable();
		
		/**
		 * \brief Resets the table with a new fitness list 
		 * \param Rank matrix of fitnesses
		 */
		void initializeTable(vector< vector<float> > fitness_list);
		
		/**
		 * \brief Selects from the table using a random rank-proportionate algorithm
		 * Note: Selection range determined by Parameters::selection_percentage
		 * \param r_seed The random seed used for rand_r
		 * \return The element selected
		 */
		GlobalFitnessLookupTableElement selectFromTable(unsigned int &r_seed);
		
		/**
		 * \brief Returns a vector of the best elements in the table (for use for determining elites)
		 * \param count Number of elements to return.
		 * \return Vector of the best elements, sorted by fitness
		 */
		vector<GlobalFitnessLookupTableElement> getBestElements(int count);
		
		/**
		 * \brief Returns the best element in the global population pool
		 */
		GlobalFitnessLookupTableElement getBestElement();
		
		/**
		 * \brief Converts a vector of table elements to a matrix used for MPIManager
		 * \param table The vector of table elements
		 * \return A 2D matrix where the first dimension represents rank and the second dimension 
		 *         corresponds to local_index
		 */
		static vector< vector<int> > TransformToRankMatrix(vector<GlobalFitnessLookupTableElement> table);
	
};
#endif
