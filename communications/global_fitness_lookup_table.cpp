#include "communications/global_fitness_lookup_table.hpp"

/* BEGIN TABLE ELEMENT CODE */

GlobalFitnessLookupTableElement::GlobalFitnessLookupTableElement(int rank, int index, float fitness)
{
	this->rank = rank;
	this->local_index = index;
	this->fitness = fitness;
}

unsigned int GlobalFitnessLookupTableElement::getRank() const
{
	return this->rank;
}

unsigned int GlobalFitnessLookupTableElement::getLocalIndex() const
{
	return this->local_index;
}

float GlobalFitnessLookupTableElement::getFitness() const
{
	return this->fitness;
}

bool GlobalFitnessLookupTableElement::operator<(const GlobalFitnessLookupTableElement &right) const
{
	return (this->getFitness() < right.getFitness());
}

/* BEGIN TABLE CODE */
GlobalFitnessLookupTable::GlobalFitnessLookupTable()
{
	this->is_sorted = false;
	this->ranking_sum = 0.0f;
	
	//precalculate the ranking sum
	int number_of_elements = Parameters::selection_percentage * Parameters::population_size;
	for(int i = 0; i < number_of_elements; i++)
	{
		this->ranking_sum += (number_of_elements - i) * (number_of_elements - i);
	}
}

void GlobalFitnessLookupTable::initializeTable(vector< vector<float> > fitness_list)
{
	elements.clear();
	elements.reserve(Parameters::population_size);
	
	this->is_sorted = false;
	
	for(unsigned r = 0; r < fitness_list.size(); r++)
	{
		for(unsigned i = 0; i < fitness_list[r].size(); i++)
		{
			GlobalFitnessLookupTableElement element(r, i, fitness_list[r][i]);
			elements.push_back(element);
		}
	}
}

void GlobalFitnessLookupTable::sortTableByFitness()
{
	//uses default < operator
	if(!this->is_sorted)
	{
		sort(elements.begin(), elements.end());
		is_sorted = true;
	}
}

vector<GlobalFitnessLookupTableElement> GlobalFitnessLookupTable::getBestElements(int count)
{
	this->sortTableByFitness();
	vector<GlobalFitnessLookupTableElement> ret_elements;
	ret_elements.reserve(count);
	ret_elements.insert(ret_elements.begin(),elements.begin(), elements.begin()+count);
	return ret_elements;
}

GlobalFitnessLookupTableElement GlobalFitnessLookupTable::getBestElement()
{
	this->sortTableByFitness();
	return elements[0];
}

GlobalFitnessLookupTableElement GlobalFitnessLookupTable::selectFromTable(unsigned int &r_seed)
{
	unsigned int stop_point = RAND_RI_RANGE(r_seed, 0, this->ranking_sum);
	unsigned int sum = 0;
	int number_of_elements = Parameters::selection_percentage * Parameters::population_size;
	for(int i = 0; i < number_of_elements; i++)
	{
		sum += (number_of_elements - i) * (number_of_elements - i);
		if(stop_point < sum) return elements[i];
	}
	FAULT("Table Stop Point overrun!");
	return GlobalFitnessLookupTableElement(0,0,0);
}

vector< vector<int> > GlobalFitnessLookupTable::TransformToRankMatrix(vector<GlobalFitnessLookupTableElement> table)
{
	vector< vector<int> > rank_matrix;
	rank_matrix.resize(MPIManager::GetCommSize());
	
	for(unsigned i = 0; i < table.size(); i++)
	{
		rank_matrix[table[i].getRank()].push_back(table[i].getLocalIndex());
	}
	
	return rank_matrix;
}
	
	
