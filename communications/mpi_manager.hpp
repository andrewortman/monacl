#ifndef __MONACL_MPIMANAGER__
#define __MONACL_MPIMANAGER__

#include "global.hpp"
#include "communications/response_headers.hpp"
#include "data/reconstruction.hpp"

#define MPI_TAG_RESULTS 0 
#define MPI_TAG_RESPONSE_HEADER 1
#define MPI_TAG_COMMANDS 2 
#define MPI_TAG_REDISTRIBUTE 4
#define MPI_TAG_SENDBACK_RECONSTRUCTION 5
#define MPI_TAG_BROADCAST_DIFFMAP 6

#define MPI_ROOT_RANK 0

class MPIManager
{	
	private:
		static int rank;
		static int size;
		
		//MPI Datatypes
		static MPI_Datatype MPI_RECONSTRUCTION_DATATYPE;
		static MPI_Datatype MPI_RESPONSEHEADERS_DATATYPE;
		
		//Temp buffers
		static Reconstruction send_back_buffer;
		static vector<float> results_buffer;

	public:
		/**
		 * \brief Initializes the MPI backend. 
		 * \param argc a reference to the program's argument count
		 * \param argv a reference to the program's argument list
		 */
		static void Initialize(int* argc, char*** argv);

		/**
		 * \brief Returns the communication rank for this process
		 */
		static int GetCommRank();

		/**
		 * \brief Returns the communication size for the world communication group
		 */
		static int GetCommSize();
		
		/**
		 * \brief Cleanly shuts down MPIManager
		 */
		static void Finish();
		
		/**
		 * \brief Sends fitness results list back to root
		 * \param results 1D array representing the fitness for each reconstructin
		 */
		static void Node_SendResults(vector<float> results);

		/**
		 * \brief Recieves all of the results from all of nodes
		 * \return 2D array representing the fitness lists for each process
		 * \param lengths reconstruction set lengths for each node
		 */
		static vector< vector<float> > Root_ReceiveResults(vector<unsigned int> lengths);
		
		/**
		 * \brief Sends a response (both a header+command list) to a specific rank.
 		 * In this method, the command count is automatically updated before sending out
		 * \param rank The target rank 
		 * \param headers Prepopulated response headers. 
		 * \param elite_commands A list of local indices to move onto the next generation w/o mutation
		 * \param mutation_commands A list of local indices to move onto the next generation w/ mutation
		 */
		static void Root_SendResponse(int rank, ResponseHeaders &headers, vector<int> &elite_commands, vector<int> &mutation_commands);

		/**
		 * \brief Receives a response from root
		 * \return Response Headers for this response message
		 * \param elite_commands Outputs a list of local indices to pass on to 
		 * 	next generation w/o mutation_commands	
		 * \param mutation_commands Outputs a list of local indices to pass on to 
		 *	next generation w/ mutations
		 */
		static ResponseHeaders Node_ReceiveResponse(vector<int> &elite_commands, vector<int> &mutation_commands);

		/**
		 * \brief Sends a reconstruction back to root
		 * \param recon The reconstruction to send back
		 */
		static void Node_SendReconstruction(Reconstruction recon);
	
		/** 
		 * \brief Receives a single reconstruction from a node
		 * \param from the node rank to recv from.
		 */
		static Reconstruction Root_ReceiveReconstruction();

		/**
		 * \brief Sends a chunk of reconstructions to another node for redistribution
		 * \param to the process rank to send the reconstructions to
		 * \param recons The list of reconstructions to send
		 */
		static void Any_SendRedistributionReconstructionList(int to, vector<Reconstruction> recons);
		
		/**
		 * \brief Recvs a chunk of reconstructions to another node for redistribution
		 * \param from The process rank to recv the reconstructions from
		 * \param count The number of reconstructions to recv
		 * \return The vector of reconstructions recieved from the node
		 */
		static vector<Reconstruction> Any_RecvRedistributionReconstructionList(int from, int count);
		
		/**
		 * \brief Broadcasts (from root) the global diff_map to all of the nodes
		 * \param diff_map A reference to the diff_map to send/recv
		 * \param diff_map_size The expected size of the diff map.
		 */
		static void Any_BroadcastDiffMap(vector<float> &diff_map);
		
};

#endif
