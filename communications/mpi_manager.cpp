#include "communications/mpi_manager.hpp"

// Initialize static variables
int MPIManager::rank = 0;
int MPIManager::size = 0;
MPI_Datatype MPIManager::MPI_RECONSTRUCTION_DATATYPE;
MPI_Datatype MPIManager::MPI_RESPONSEHEADERS_DATATYPE;
Reconstruction MPIManager::send_back_buffer;
vector<float> MPIManager::results_buffer;


void MPIManager::Initialize(int* argc, char*** argv)
{
	HANDLE_MPI_ERROR(MPI_Init(argc, argv));
	HANDLE_MPI_ERROR(MPI_Comm_rank(MPI_COMM_WORLD, &MPIManager::rank));
	HANDLE_MPI_ERROR(MPI_Comm_size(MPI_COMM_WORLD, &MPIManager::size));
	
	MPIManager::MPI_RECONSTRUCTION_DATATYPE = Reconstruction::GetMPIType();
	MPIManager::MPI_RESPONSEHEADERS_DATATYPE = ResponseHeaders::GetMPIType();
	DEBUG("MPI Initialized (rank = %i, size = %i)", rank, size);
}

int MPIManager::GetCommRank()
{
	return MPIManager::rank;
}

int MPIManager::GetCommSize()
{
	return MPIManager::size;
}

void MPIManager::Finish()
{
	HANDLE_MPI_ERROR(MPI_Finalize());
}

void MPIManager::Node_SendResults(vector<float> results)
{
	MPI_Request not_used;
	MPIManager::results_buffer = results;
	DEBUG("Rank %i sending results", rank);
	HANDLE_MPI_ERROR(MPI_Isend(&results_buffer.front(), results.size(), MPI_FLOAT, 
			MPI_ROOT_RANK, MPI_TAG_RESULTS, MPI_COMM_WORLD, &not_used));
}

vector< vector<float> > MPIManager::Root_ReceiveResults(vector<unsigned int> lengths)
{
	ASSERT(lengths.size() == (unsigned)MPIManager::GetCommSize(), 
			"Invalid local set lengths vector. Length size = %lu, Comm Size = %i", 
			lengths.size(), MPIManager::GetCommSize());
	
	vector< vector<float> > all_results;
	vector<MPI_Request> requests;
	all_results.resize(MPIManager::GetCommSize());
	requests.resize(MPIManager::GetCommSize());
	for(int i = 0; i < MPIManager::GetCommSize(); i++)
	{
		all_results[i].resize(lengths[i]);
		HANDLE_MPI_ERROR(MPI_Irecv(&all_results[i].front(), lengths[i], MPI_FLOAT, 
				i, MPI_TAG_RESULTS, MPI_COMM_WORLD, &requests[i]));
	}

	MPI_Waitall(MPIManager::GetCommSize(), &requests.front(), MPI_STATUSES_IGNORE);
	return all_results;
}

void MPIManager::Root_SendResponse(int rank, ResponseHeaders &headers, vector<int> &elite_commands, vector<int> &mutation_commands)
{
	ASSERT(elite_commands.size() == headers.getNumberOfEliteCommands(), "Invalid elite command size");
	ASSERT(mutation_commands.size() == headers.getNumberOfMutationCommands(), "Invalid mutation command size");
	
	//first, send the headers out to the node. This lets the node what to expect and
	//any other commands like "send back" and "redistribute"
	MPI_Request not_used;
	HANDLE_MPI_ERROR(MPI_Isend(&headers, 1, MPIManager::MPI_RESPONSEHEADERS_DATATYPE, 
								rank, MPI_TAG_RESPONSE_HEADER, MPI_COMM_WORLD, &not_used));
	
	
	//merge individual command list to single vector 
	//elite first 
	vector<int> all_commands;
	all_commands.resize(headers.getNumberOfCommands());
	
	copy(elite_commands.begin(), elite_commands.end(), all_commands.begin());
	copy(mutation_commands.begin(), mutation_commands.end(), all_commands.begin()+elite_commands.size());
	
	//Now send out thatvector to the node. This way we dont have to send two messages, we just clump
	//the elite list and the mutation list all at once.
	HANDLE_MPI_ERROR(MPI_Isend(&all_commands.front(), headers.getNumberOfCommands(), MPI_INT, 
								rank, MPI_TAG_COMMANDS, MPI_COMM_WORLD, &not_used));
}

ResponseHeaders MPIManager::Node_ReceiveResponse(vector<int> &elite_commands, vector<int> &mutation_commands)
{
	ResponseHeaders rh;
	HANDLE_MPI_ERROR(MPI_Recv(&rh, 1, MPIManager::MPI_RESPONSEHEADERS_DATATYPE, MPI_ROOT_RANK, 
						MPI_TAG_RESPONSE_HEADER, MPI_COMM_WORLD, NULL));
			
	elite_commands.resize(rh.getNumberOfEliteCommands());
	mutation_commands.resize(rh.getNumberOfMutationCommands());
	
	DEBUG("Received response headers from root (rank %i)",MPIManager::GetCommRank());
	vector<int> all_commands;
	all_commands.resize(rh.getNumberOfCommands());
	HANDLE_MPI_ERROR(MPI_Recv(&all_commands.front(), rh.getNumberOfCommands(), MPI_INT, 
								MPI_ROOT_RANK, MPI_TAG_COMMANDS, MPI_COMM_WORLD, NULL));
								
	DEBUG("Recieved %i elite commands, %i mutation commands (rank %i)", rh.getNumberOfEliteCommands(),
			rh.getNumberOfMutationCommands(), MPIManager::GetCommRank());
	
	//fill the individual command buffers from the combined command buffers		
	//elite commands first, mutation commands second
	elite_commands.resize(rh.getNumberOfEliteCommands());
	mutation_commands.resize(rh.getNumberOfMutationCommands());
	
	copy(all_commands.begin(), all_commands.begin()+rh.getNumberOfEliteCommands(), elite_commands.begin());
	copy(all_commands.begin()+rh.getNumberOfEliteCommands(), all_commands.end(), mutation_commands.begin());
	
	return rh;
}

void MPIManager::Node_SendReconstruction(Reconstruction recon)
{
	MPI_Request not_used;
	send_back_buffer = recon;
	HANDLE_MPI_ERROR(MPI_Isend(&MPIManager::send_back_buffer, 1, MPIManager::MPI_RECONSTRUCTION_DATATYPE, MPI_ROOT_RANK,
					 MPI_TAG_SENDBACK_RECONSTRUCTION, MPI_COMM_WORLD, &not_used));
	DEBUG("Rank %i sent reconstruction to root", MPIManager::GetCommRank());
}

Reconstruction MPIManager::Root_ReceiveReconstruction()
{
	Reconstruction recon;
	HANDLE_MPI_ERROR(MPI_Recv(&recon, 1, MPIManager::MPI_RECONSTRUCTION_DATATYPE, MPI_ANY_SOURCE,
						MPI_TAG_SENDBACK_RECONSTRUCTION, MPI_COMM_WORLD, NULL));
	DEBUG("Root received reconstruction.");
	return recon;
}

void MPIManager::Any_SendRedistributionReconstructionList(int to, vector<Reconstruction> recons)
{
	HANDLE_MPI_ERROR(MPI_Send(&recons.front(), recons.size(), MPIManager::MPI_RECONSTRUCTION_DATATYPE,
								to, MPI_TAG_REDISTRIBUTE, MPI_COMM_WORLD));
	DEBUG("Sent redistribution: %i -> %i (size %lu)", MPIManager::GetCommRank(), to, recons.size());
}

vector<Reconstruction> MPIManager::Any_RecvRedistributionReconstructionList(int from, int count)
{
	vector<Reconstruction> reconstructions;
	reconstructions.resize(count);
	HANDLE_MPI_ERROR(MPI_Recv(&reconstructions.front(), count, MPIManager::MPI_RECONSTRUCTION_DATATYPE,
								from, MPI_TAG_REDISTRIBUTE, MPI_COMM_WORLD, NULL));
								
	return reconstructions;
}

void MPIManager::Any_BroadcastDiffMap(vector<float> &diff_map)
{
	//This method uses MPI Broadcast. If the method is called via the root rank, then diff_map is sent out 
	//to all nodes and if the method is called from a node, it used to receive from root
	int diff_map_size = Parameters::diff_grid_x * Parameters::diff_grid_y;
	
	//Initially, the diffmap vector size is 0 (so the first generation can run without problem)
	//But, if this method is called and we are a node, then we need to resize the array so we have
	//buffer space.
	if(diff_map.size() == 0) diff_map.resize(diff_map_size);
	
	HANDLE_MPI_ERROR(MPI_Bcast(&diff_map.front(), diff_map_size, MPI_FLOAT, MPI_ROOT_RANK, MPI_COMM_WORLD));
}
