#ifndef __MONACL_RESPONSEHEADERS_HPP__
#define __MONACL_RESPONSEHEADERS_HPP__

#include "global.hpp"
#include "mpi.h"

//Response Header Methods (bit flags)
#define SEND_BACK_RECONSTRUCTION (1<<0)
#define REDISTRIBUTE_NONE ((1<<1) | (1<<2))
#define REDISTRIBUTE_SEND (1<<1)
#define REDISTRIBUTE_RECV (1<<2)

//For MPI Types, you should really use a C struct and build a wrapper class around it
//This is for compatibility with the older C++ standard
typedef struct
{
		char methods;
		unsigned int number_of_mutation_commands;
		unsigned int number_of_elite_commands;
		unsigned int redistribute_partner;
		unsigned int redistribute_count;
		unsigned int sendback_recon_index;
} response_headers_t;

class ResponseHeaders : private response_headers_t
{
	private:
	

	public:
		/**
		 * @brief Default constructor - sets everything to 0 
		 */
		ResponseHeaders();
		
		/**
		 * Tells the receiver of this message to send back the reconstruction at sendback_index
		 * back to root
		 * @brief Sets the sendback index
		 * @param sendback_index The 0-based local index of the reconstruction to be sent back
		 */
		void setIsToSendBackReconstruction(unsigned int sendback_index);
		
		/**
		 * Sets the number of mutation commands to expect during the command message
 		 * @param number the count of mutation (only) commands
		 */
		void setNumberOfMutationCommands(unsigned int number);
		
		/**
		 * Sets the number of elite commands to expect during the command message
		 * @param number the count of elite (only) commands
		 */
		void setNumberOfEliteCommands(unsigned int number);
		
		/**
		 * Tells the reciever to redistribute by sending to partner_rank
	         * @param partner_rank the partner to redistribute with
		 * @param count the number of reconstructions to send
		 */
		void setIsToSendRedistribute(unsigned int partner_rank, unsigned int count);
		
		/**
		 * Tells the reciever to redistribute by receiving to partner_rank
	         * @param partner_rank the partner to redistribute with
		 * @param count the number of reconstructions to expect
		 */
		void setIsToRecvRedistribute(unsigned int partner_rank, unsigned int count);

		/**
		 * Returns true if the response header indicates the receiver is to 
		 * send back a reconstruction to root
		 */
		bool isToSendBackReconstruction();

		/**
		 * Returns true if the response header indicates the receiver is to 
		 * redistribute (either send or recv)
		 */
		bool isToRedistribute();
		
		/**
		 * Returns true if the response header indicates the receiver is to 
		 * redistribute via send
		 */
		bool isToSendRedistribute();

		/**
		 * Returns true if the response header indicates the receiver is to 
		 * redistribute via recv
		 */
		bool isToRecvRedistribute();

		/**
		 * Returns the total number of mutation-only commands to expect 
		 * in the command list
		 */
		unsigned int getNumberOfMutationCommands();

		/**
		 * Returns the total number of elite-only commands to expect
		 * in the command list
		 */
		unsigned int getNumberOfEliteCommands();

		/**
		 * Returns the total number of commands (both mutation+elite) to expect
 		 * in the command list
		 */
		unsigned int getNumberOfCommands();

		/**
		 * Returns the partner rank to use if redistribution is to be performed
		 */
		unsigned int getRedistributePartnerRank();

		/**
		 * Returns the number of reconstructions to send/recv during redistributions	
		 */
		unsigned int getRedistributeCount();

		/**
		 * Returns the local index of the reconstruction to be sent back to root
		 * if necessary 
		 */
		unsigned int getSendBackIndex();

		/**
		 * Generates an MPI Datatype for ResponseHeaders
		 * Note: I'm not sure if this can be safely called multiple times
		 */
		static MPI_Datatype GetMPIType();
};
#endif
