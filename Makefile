CPPC=g++
CC=gcc
LDFLAGS=-lm -fopenmp $(shell mpic++ --showme:link)
CPPFLAGS=-fopenmp $(shell mpic++ --showme:compile) -I/usr/local/cuda/include -I./
CFLAGS=-fopenmp $(shell mpic++ --showme:compile) -I/usr/local/cuda/include

DEBUG_FLAGS=-D__MONACL_DEBUG__ -D__MONACL_INFO__ -g -Wall
INFO_FLAGS=-D__MONACL_INFO__ -Wall
RELEASE_FLAGS= 

TARGET=monacl
OBJS=monacl.o device.o polygon.o reconstruction.o lodepng.o fitness_job.o render_job.o difference_map_job.o image_renderer.o compute_manager.o mpi_manager.o parameters.o mongoose.o mongoose_wrapper.o embedded_webserver.o response_headers.o global_fitness_lookup_table.o distribution_manager.o


default: linux_release

release: build

mac_debug: LDFLAGS+=-framework OpenCL
mac_debug: CPPFLAGS+=$(DEBUG_FLAGS)
mac_debug: CFLAGS+=$(DEBUG_FLAGS)
mac_debug: build

mac_release: LDFLAGS+=-framework OpenCL
mac_release: CPPFLAGS+=$(RELEASE_FLAGS)
mac_release: CFLAGS+=$(RELEASE_FLAGS)
mac_release: build

linux_debug: LDFLAGS+=-lOpenCL
linux_debug: CPPFLAGS+=$(DEBUG_FLAGS)
linux_debug: CFLAGS+=$(DEBUG_FLAGS)
linux_debug: build

linux_info:	LDFLAGS+=-lOpenCL
linux_info: CPPFLAGS+=$(INFO_FLAGS)
linux_info: CFLAGS+=$(INFO_FLAGS)
linux_info: build

linux_release: LDFLAGS+=-lOpenCL
linux_release: CPPFLAGS+=$(RELEASE_FLAGS)
linux_release: CFLAGS+=$(RELEASE_FLAGS)
linux_release: build

build: $(OBJS)
	$(CPPC) $(OBJS) $(LDFLAGS) -o $(TARGET)

monacl.o: monacl.cpp
	$(CPPC) $(CPPFLAGS) -c monacl.cpp

device.o: computation/device.cpp
	$(CPPC) $(CPPFLAGS) -c computation/device.cpp

polygon.o: data/polygon.cpp
	$(CPPC) $(CPPFLAGS) -c data/polygon.cpp

reconstruction.o: data/reconstruction.cpp
	$(CPPC) $(CPPFLAGS) -c data/reconstruction.cpp

lodepng.o: helpers/lodepng.cpp
	$(CPPC) $(CPPFLAGS) -c helpers/lodepng.cpp

fitness_job.o: computation/fitness_job.cpp
	$(CPPC) $(CPPFLAGS) -c computation/fitness_job.cpp
	
render_job.o: computation/render_job.cpp
	$(CPPC) $(CPPFLAGS) -c computation/render_job.cpp
	
difference_map_job.o: computation/difference_map_job.cpp
	$(CPPC) $(CPPFLAGS) -c computation/difference_map_job.cpp
	
compute_manager.o: computation/compute_manager.cpp
	$(CPPC) $(CPPFLAGS) -c computation/compute_manager.cpp

parameters.o: helpers/parameters.cpp
	$(CPPC) $(CPPFLAGS) -c helpers/parameters.cpp

mongoose.o: helpers/mongoose.c
	$(CC) $(CFLAGS) -c helpers/mongoose.c

mongoose_wrapper.o: helpers/mongoose_wrapper.cpp
	$(CPPC) $(CPPFLAGS) -c helpers/mongoose_wrapper.cpp

embedded_webserver.o: helpers/embedded_webserver.cpp
	$(CPPC) $(CPPFLAGS) -c helpers/embedded_webserver.cpp

image_renderer.o: helpers/image_renderer.cpp
	$(CPPC) $(CPPFLAGS) -c helpers/image_renderer.cpp

mpi_manager.o: communications/mpi_manager.cpp
	$(CPPC) $(CPPFLAGS) -c communications/mpi_manager.cpp

response_headers.o: communications/response_headers.cpp
	$(CPPC) $(CPPFLAGS) -c communications/response_headers.cpp

global_fitness_lookup_table.o: communications/global_fitness_lookup_table.cpp
	$(CPPC) $(CPPFLAGS) -c communications/global_fitness_lookup_table.cpp
	
distribution_manager.o: communications/distribution_manager.cpp
	$(CPPC) $(CPPFLAGS) -c communications/distribution_manager.cpp
	
docs:
	doxygen Doxyfile

clean:
	rm -rf $(OBJS) $(TARGET)
	rm -rf visible/images/outputs/svg/*.svg
	rm -rf visible/images/outputs/png/*.png
	rm -rf docs
