#include "global.hpp"
#include "data/data_structs.hpp"
#include "computation/device.hpp"
#include "data/reconstruction.hpp"
#include "data/polygon.hpp"
#include "communications/global_fitness_lookup_table.hpp"
#include "helpers/lodepng.hpp"
#include "computation/compute_manager.hpp"
#include "helpers/embedded_webserver.hpp"
#include "helpers/image_renderer.hpp"
#include "communications/response_headers.hpp"
#include "communications/mpi_manager.hpp"
#include "communications/distribution_manager.hpp"
#include "mpi.h"

vector<Reconstruction> createInitialReconstructionPool(cl_int length, vector<cl_uint> &random_seeds, rgba_t* input_png);
static inline double getTimeDifference(const struct timespec a, const struct timespec b);

int main(int argc, char** argv)
{
	//INITIALIZE MPI
	int num_proc, rank;
	
	struct timespec start_time;
	clock_gettime(CLOCK_MONOTONIC, &start_time);
	
	
	MPIManager::Initialize(&argc, &argv);
	rank = MPIManager::GetCommRank();
	num_proc = MPIManager::GetCommSize();

	//READ ARGUMENTS
	cl_device_type dev_type = CL_DEVICE_TYPE_ALL;
	cl_uint target_gen_num = 0;
	if(argc > 1) 
	{
		if(strcmp(argv[1],"gpu") == 0)
			dev_type = CL_DEVICE_TYPE_GPU;
		else if(strcmp(argv[1], "cpu") == 0)
			dev_type = CL_DEVICE_TYPE_CPU;
		else if(strcmp(argv[1],"both") == 0)
			dev_type = CL_DEVICE_TYPE_GPU|CL_DEVICE_TYPE_CPU;
		else
			FAULT("Invalid device type (gpu,cpu,or both)");

		if(argc > 2)
		{
			target_gen_num = (cl_uint)atoi(argv[2]);
			if(rank == 0) 
				INFO("Target generation set at %u", target_gen_num);
			
		}
		else
		{
			target_gen_num = 0;
			if(rank == 0)
				INFO("Target generation set to UNLIMITED");
		}
	}
	else
	{
		FAULT("Arguments: %s gpu|cpu|both [target generation number]", argv[0]);
	}
	
	//LOAD PARAMETERS FROM CONF FILE
	Parameters::loadFromFile("parameters.conf");
	
	//SETUP DISTRIBUTION MANAGER
	if(MPIManager::GetCommRank() == 0) DistributionManager::Initialize();
	
	//START WEBSERVER IF USED
	EmbeddedWebServer embeddedWebserver;
	if(rank == 0)	
	{
    		INFO("Starting embedded webserver on port %i", Parameters::webserver_port);
  
    		//EmbeddedWebServer embeddedWebserver;    
    		std::stringstream portStrStream;
   			portStrStream << Parameters::webserver_port;
    		embeddedWebserver.setOption("document_root", "visible");
    		embeddedWebserver.setOption("listening_ports", portStrStream.str());
    		embeddedWebserver.setOption("num_threads", "5");    
    		embeddedWebserver.start();
    	
    		INFO("Embedded webserver started!");
   	}
	 
	
	//READ IN TARGET IMAGE!
	unsigned char* png_raw;
	if(LodePNG_decode32_file(&png_raw, &Parameters::image_width, &Parameters::image_height, "visible/images/input.png") != 0)
	{
		FAULT("Could not load input.png");
	}
	rgba_t *target_image = (rgba_t*)png_raw;
	
	//CREATE RANDOM NUMBER SEEDS FOR OMP THREADS
	cl_uint _t = time(NULL);
	vector<cl_uint> random_seeds;
	DEBUG("Num threads used during random generation process = %u", (unsigned)omp_get_max_threads());
	random_seeds.resize(omp_get_max_threads());
	for(int i = 0; i < omp_get_max_threads(); i++)	random_seeds[i] = (rank+1)*(_t + i);
	
	//INITIALIZE THE COMPUTE MANAGER
	vector<Device> avaliable_devices = Device::GetAvaliableDevices(dev_type);
	INFO("There are %i devices avaliable.", (int)avaliable_devices.size());   
	ComputeManager::Initialize(avaliable_devices, "computation/compute_kernels.cl", target_image);
	
	//CREATE INITIAL (RANDOM) POPULATION
	vector<Reconstruction> local_recons;
	//dont worry if the population_size is not evenly divisible, that is handled in the distributionmanager
	local_recons.resize(Parameters::population_size/MPIManager::GetCommSize());
	local_recons = createInitialReconstructionPool(local_recons.size(), random_seeds, (rgba_t*)png_raw);
	
	//SETUP DIFFERENCE MAP
	vector<float> diff_map;
	
	
	//MAIN LOOP
	cl_uint gen_num = 0; //generation number (first generation is '1')
	while(target_gen_num == 0 || gen_num < target_gen_num) //(always run if the target = 0 or we reach a target)
	{	
		//NODE JOBS (PART ONE)
		
		gen_num++; //generation number increment
        
        //Compute the current generation (the first generation is randomized, see above)
		INFO("Rank %i starting calculation for generation %i [size=%lu]", rank, gen_num, local_recons.size());
		vector<float> local_results = ComputeManager::calculateFitnesses(local_recons);
		
		//Send the fitness list back to root for processing
		MPIManager::Node_SendResults(local_results);
		
		//ROOT JOBS (PART ONE)
		//Now time for root to collect results and do stuff with it
		if(rank == 0)
		{	
			//Use a 2D vector to store the fitness results by the ranks
			vector< vector<float> > global_fitness_results; 
			global_fitness_results = MPIManager::Root_ReceiveResults(DistributionManager::GetLocalSetSizes());
			
			//Create the global fitness table
			GlobalFitnessLookupTable gflt;
			gflt.initializeTable(global_fitness_results);
			
			//Get the best element so we can record time and so that
			// it can be sent back to root
			GlobalFitnessLookupTableElement best_element = gflt.getBestElement();
			INFO("Best fitness was located at rank %i with a fitness of %f", best_element.getRank(), best_element.getFitness());
			
			struct timespec end_time;
			clock_gettime(CLOCK_MONOTONIC, &end_time);
			
			OUTPUTLINE("%i, %i, %f, %lf", gen_num, best_element.getRank(), best_element.getFitness(), getTimeDifference(start_time, end_time));
			
			
			//Go ahead and grab the elites
			int number_of_elites = (int)(Parameters::elitism_percentage * Parameters::population_size);
			vector<GlobalFitnessLookupTableElement> elites = 
				gflt.getBestElements(number_of_elites);
			
			//Create global mutation list
			vector<GlobalFitnessLookupTableElement> mutations;
			mutations.reserve(Parameters::population_size-number_of_elites);
			#pragma omp parallel shared(mutations) num_threads(omp_get_max_threads())
			{
				#pragma omp for
				for(int i = 0; i < Parameters::population_size-number_of_elites; i++)
				{
					GlobalFitnessLookupTableElement element = 
							gflt.selectFromTable(random_seeds[omp_get_thread_num()]);
					
					#pragma omp critical
					mutations.push_back(element);
				}
			}
			
			//Convert into rank matrix for easier processing
			vector< vector<int> > mutations_by_rank = GlobalFitnessLookupTable::TransformToRankMatrix(mutations);
			vector< vector<int> > elites_by_rank = GlobalFitnessLookupTable::TransformToRankMatrix(elites);
			
			//Now create the response headers for each node.
			
			//Create a vector of responseheaders for futher modification by the distributionmanager
			vector< ResponseHeaders> headers;
			headers.resize(MPIManager::GetCommSize());
			
			
			//Now we have a list of mutations and elites (by rank) lets send out the commands
			for(int i = 0; i < MPIManager::GetCommSize(); i++)
			{
				ResponseHeaders rh;
				rh.setNumberOfEliteCommands(elites_by_rank[i].size());
				rh.setNumberOfMutationCommands(mutations_by_rank[i].size());
				
				//if this rank has the best element, lets tell them to send it back to us
				if(best_element.getRank() == (unsigned)i)
				{
					DEBUG("Asking rank %i to send back recon #%u.", i, best_element.getLocalIndex());
					rh.setIsToSendBackReconstruction(best_element.getLocalIndex());
				}
					
				headers[i] = rh;
			}
			
			//let the DM do its magic by determining redistribution and modifying the headers accordingly
			//AND updating the local set size correctly for use when receiving results.
			DistributionManager::HandleRedistribution(headers);
			
			//Now send out the header+commands
			for(int i = 0; i < MPIManager::GetCommSize(); i++)
			{
				MPIManager::Root_SendResponse(i, headers[i], elites_by_rank[i], mutations_by_rank[i]);
			}	
		} //end root jobs (part one)
		
		//NODE JOBS (PART TWO)
		//Receive the response from root
		vector<int> mutations, elites;
		ResponseHeaders rh = MPIManager::Node_ReceiveResponse(elites, mutations);
		DEBUG("Rank %i received %i commands from root!", MPIManager::GetCommRank(), rh.getNumberOfCommands());
		
		//If we have the best reconstruction, send it back to root so it can store it on disk for logging
		if(rh.isToSendBackReconstruction())
		{
			MPIManager::Node_SendReconstruction(local_recons[rh.getSendBackIndex()]);
		}
		
		//Creates a new local population using the commands received from root
		vector<Reconstruction> new_local_recons;
		new_local_recons.reserve(rh.getNumberOfCommands());
		//first, take the elites out and put them in the list
		for(unsigned i = 0; i < rh.getNumberOfEliteCommands(); i++)
		{
			new_local_recons.push_back(local_recons[elites[i]]);
		}
		
		//next, perform the mutations accordingly
		#pragma omp parallel shared(mutations) num_threads(omp_get_max_threads())
		{
			#pragma omp for 
			for(unsigned i = 0; i < rh.getNumberOfMutationCommands(); i++)
			{
				Reconstruction new_recon = local_recons[mutations[i]];
				new_recon.mutate(random_seeds[0], diff_map);
		
				#pragma omp critical
				new_local_recons.push_back(new_recon);
			}
		}
		
		//now redistribute if neccessary
		if(rh.isToRecvRedistribute())
		{
			vector<Reconstruction> redist_recons = 
				MPIManager::Any_RecvRedistributionReconstructionList(rh.getRedistributePartnerRank(),
																	 rh.getRedistributeCount());
			
			new_local_recons.insert(new_local_recons.end(), redist_recons.begin(), redist_recons.end());
		}
		else if(rh.isToSendRedistribute())
		{
			vector<Reconstruction> to_send;
			
			//We want a fairly decent representation of elites and mutations in 
			//the set we send. So shuffle the deck first
			
			random_shuffle(new_local_recons.begin(), new_local_recons.end());
			
			to_send.insert(to_send.begin(), new_local_recons.begin(),
						   new_local_recons.begin()+rh.getRedistributeCount());
			new_local_recons.erase(new_local_recons.begin(), new_local_recons.begin()+rh.getRedistributeCount());
			
			MPIManager::Any_SendRedistributionReconstructionList(rh.getRedistributePartnerRank(), to_send);
		}
		
		DEBUG("New local recon size for rank %i is %lu", MPIManager::GetCommRank(), new_local_recons.size());
		local_recons = new_local_recons; //set the next generation recon list to be the just generated list
		
		//OK, back to root for more processing (part two)
		if(rank == 0) 
		{
			//Receive the reconstruction and save it as SVG to disk
			Reconstruction best_recon = MPIManager::Root_ReceiveReconstruction();
			stringstream filename_svg;
			filename_svg << "visible/images/outputs/svg/" << "gen_" << setfill('0') << setw(5) << gen_num << ".svg";
			ImageRenderer::saveReconstructionToSVG(best_recon, filename_svg.str());
			
			//With the best reconstruction in hand, see if we need to recalculate the diffmap
			if(Parameters::diff_map_gen_count != 0 && 
				(gen_num-1)%Parameters::diff_map_gen_count == 0)
			{
				INFO("Root generating diffmap..");
				diff_map = ComputeManager::calculateDifferenceMap(best_recon);
			}
			
			//finally, as root, lets add the generation info to the webserver database
			embeddedWebserver.addGenerationToStats(gen_num, best_recon.fitness, best_recon.num_polygons);
		} //end root job (part two)
		
		//NODE JOBS (PART THREE) START
		
		//Check to see if diffmap needs to be refreshed globally
		if(Parameters::diff_map_gen_count != 0 && 
				(gen_num-1)%Parameters::diff_map_gen_count == 0)
		{
			DEBUG("Rank %i diffmap broadcast underway.", MPIManager::GetCommRank());			
			MPIManager::Any_BroadcastDiffMap(diff_map);
			DEBUG("Rank %i received diffmap", MPIManager::GetCommRank());
		}
		
		//end node jobs (part three)
	} //MAIN LOOP
		
	//We completed the number of generations asked of us
	INFO("Process complete. Shutting down.");
	MPI_Finalize();
	INFO("Goodbye.");
}

/* Creates a random set of reconstructions for the initial generation */
vector<Reconstruction> createInitialReconstructionPool(int length, vector<cl_uint> &random_seeds, rgba_t* input_png)
{
	vector<Reconstruction> recons;
	recons.resize(length);
	
	DEBUG("Generating random intitial set of size %i", (int)recons.size());
	#pragma omp parallel shared(recons) num_threads(omp_get_max_threads())
	{
		#pragma omp for
		for(int i = 0; i < length; i++)
		{
			vector<Polygon> gons;
			gons.reserve(MAX_POLYGONS);
			for(cl_uint g = 0; g < Parameters::start_polygon_count; g++)
			{
				Polygon p = Polygon::RandomPolygon(random_seeds[omp_get_thread_num()]);
				//color hint
				//get a bounding box of p
				rgba_t color = p.color;
			
				if(Parameters::color_hint)
				{
					boundingbox_t bb = p.getBoundingBox();
			
					vertex_t center;
					center.x = (bb.max.x + bb.min.x)/2.0f;
					center.y = (bb.max.y + bb.min.y)/2.0f;
					if(center.x < 0 || center.y < 0 || center.x > 1.0f || center.y > 1.0f)
					{
						color.r = color.g = color.b = 255;
					}
					else
					{
						cl_uint xx = (cl_uint)(center.x*Parameters::image_width);
						cl_uint yy = (cl_uint)(center.y*Parameters::image_height);
						cl_uint color_idx = xx + yy*Parameters::image_width;
						color.r = input_png[color_idx].r;
						color.g = input_png[color_idx].g;
						color.b = input_png[color_idx].b;
					}
					p.color = color;
				}
				
				gons.push_back(p);
			}
			recons[i] = (Reconstruction(gons));
		}
	}
	
	DEBUG("Generated %i random recon images", (int)recons.size());
	
	return recons;
}


static inline double getTimeDifference(const struct timespec a, const struct timespec b)
{
	#define NS_IN_SEC 1000000000.0
	
    if(b.tv_nsec < a.tv_nsec)
    {        
        double result = (b.tv_sec - a.tv_sec - 1) + ((double)(NS_IN_SEC + b.tv_nsec - a.tv_nsec)/NS_IN_SEC);
        return result;
    } else
    {
        double result = (b.tv_sec - a.tv_sec) + (double)(b.tv_nsec - a.tv_nsec)/NS_IN_SEC;
        return result;
    }
}
